<?php namespace App\Console\Commands;

use App\Contracts\Storage\File;
use App\Contracts\Storage\Note;
use Illuminate\Console\Command;

class Cleanup extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'note:cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove expired and viewed notes.';

    /**
     * @var Note
     */
    protected $provider;

    /**
     * @var File
     */
    protected $fileProvider;

    public function __construct(Note $provider, File $fileProvider)
    {
        $this->provider = $provider;
        $this->fileProvider = $fileProvider;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Starting cleanup...');
        $this->info(sprintf('Removed %d expired notes.', $this->provider->removeExpired()));
        $this->info(sprintf('Removed %d viewed notes.', $this->provider->removeViewed()));
        $this->info(sprintf('Removed %d files.', $this->fileProvider->cleanup()));
        $this->comment('Cleanup completed.');
    }
}
