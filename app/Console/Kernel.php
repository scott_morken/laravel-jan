<?php namespace App\Console;

use App\Console\Commands\Cleanup;
use Illuminate\Console\Scheduling\Schedule;
use Smorken\Ext\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Cleanup::class,
        \Smorken\Ext\Console\Commands\EnvSwitch::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule
            ->command('note:cleanup')
            ->everyFiveMinutes()
            ->user(env('CRON_USER', 'www-data'))
            ->sendOutputTo(storage_path() . '/logs/cron.log')
            ->emailErrorOutputTo(explode('|', env('ERROR_EMAIL', 'email@myemail.edu')));
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
