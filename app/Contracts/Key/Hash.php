<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 9:25 AM
 */

namespace App\Contracts\Key;

use App\Contracts\Storage\Note;

/**
 * Interface Key
 * @package App\Contracts\Key
 */
interface Hash
{

    /**
     * @return string
     */
    public function getSalt();

    /**
     * @param $key
     * @param $shared
     * @param bool $throw
     * @return string
     */
    public function create($key, $shared, $throw = true);

    /**
     * @return Note
     */
    public function getProvider();

    /**
     * @param $key
     * @return bool
     */
    public function check($key);
}
