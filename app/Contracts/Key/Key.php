<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 9:35 AM
 */

namespace App\Contracts\Key;

use App\Contracts\Storage\Note;

interface Key
{

    /**
     * @return int
     */
    public function getLength();

    /**
     * @param null $length
     * @return int
     */
    public function get($length = null);
}
