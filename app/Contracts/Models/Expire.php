<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:27 AM
 */

namespace App\Contracts\Models;

/**
 * Interface Expire
 * @package App\Contracts\Models
 *
 * @property string $id
 * @property string $time_string
 * @property string $descr
 * @property string $weight
 */
interface Expire
{

}
