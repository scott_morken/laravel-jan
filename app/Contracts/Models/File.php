<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/29/17
 * Time: 8:45 AM
 */

namespace App\Contracts\Models;

/**
 * Interface File
 * @package App\Contracts\Models
 *
 * @property int $id
 * @property int $note_id
 * @property string $filename
 * @property string $mime
 * @property int $size
 * @property string $data
 *
 * @property \App\Contracts\Models\Note $note
 */
interface File
{

}
