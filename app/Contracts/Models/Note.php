<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:35 AM
 */

namespace App\Contracts\Models;

use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Interface Notes
 * @package App\Contracts\Models
 *
 * @property int $id
 * @property Carbon $expire_at
 * @property string $key
 * @property string $data
 * @property int $views
 * @property int $views_allowed
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection<\App\Contracts\Models\File> $files
 */
interface Note
{

}
