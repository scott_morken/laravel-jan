<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:11 AM
 */

namespace App\Contracts\Models;

/**
 * Interface Notification
 * @package App\Contracts\Model
 *
 * @property string $id
 * @property string $descr
 * @property string $handler
 * @property string $vector_id
 */
interface Notification
{

}
