<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 2:13 PM
 */

namespace App\Contracts\Note;

use App\Contracts\Storage\Expire;
use App\Contracts\Storage\File;
use App\Contracts\Storage\Note;
use Illuminate\Contracts\Encryption\Encrypter;

interface Builder
{
    /**
     * ['key' => [
     *      'value' => ?
     *      'notification_id' => ?
     *      'to' => ?
     *   ],
     *   'shared' => [
     *      'value' => ?
     *      'notification_id' => ?
     *      'to' => ?
     *   ],
     *   'views_allowed' => ?,
     *   'expire_at' => ?,
     *   'note' => ?,
     * ]
     * @param array $data
     * @return bool
     */
    public function build(array $data);

    /**
     * @return KeySharedBuilder
     */
    public function getKeySharedBuilder();

    /**
     * @return Note
     */
    public function getProvider();

    /**
     * @return Expire
     */
    public function getExpireProvider();

    /**
     * @return File
     */
    public function getFileProvider();

    /**
     * @return Encrypter
     */
    public function getEncryptor();

    /**
     * @return array
     */
    public function getErrors();

    /**
     * @return bool
     */
    public function hasErrors();
}
