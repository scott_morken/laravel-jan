<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/10/17
 * Time: 10:21 AM
 */

namespace App\Contracts\Note;

use App\Contracts\Models\File;
use Illuminate\Contracts\Encryption\Encrypter;

interface FileRetriever
{

    /**
     * @param $id
     * @return File
     */
    public function get($id);

    /**
     * @return Encrypter
     */
    public function getEncrypter();

    /**
     * @return \App\Contracts\Storage\File
     */
    public function getProvider();
}
