<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 2:13 PM
 */

namespace App\Contracts\Note;

use App\Contracts\Key\Hash;
use App\Contracts\Notify\Notifier;
use App\Contracts\Storage\Notification;
use App\Services\Notifiers\Factory;
use Illuminate\Contracts\Routing\UrlGenerator;

interface KeySharedBuilder
{

    public function build(array $data);

    public function getValue($type);

    /**
     * @param $type
     * @return Notifier|null
     */
    public function getNotifier($type);

    public function getTo($type);

    /**
     * @return string
     */
    public function getHash();

    public function notify();

    /**
     * @return Notification
     */
    public function getNotificationProvider();

    /**
     * @return Hash
     */
    public function getHasher();

    /**
     * @return Factory
     */
    public function getNotifierFactory();

    /**
     * @return UrlGenerator
     */
    public function getUrl();
}
