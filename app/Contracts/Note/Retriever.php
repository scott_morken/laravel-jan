<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 2:13 PM
 */

namespace App\Contracts\Note;

use App\Contracts\Key\Hash;
use App\Contracts\Storage\Note;
use Illuminate\Contracts\Encryption\Encrypter;

interface Retriever
{

    /**
     * @param $key
     * @param $shared
     * @return string
     */
    public function get($key, $shared);

    /**
     * @return Encrypter
     */
    public function getEncrypter();

    /**
     * @return Note
     */
    public function getProvider();

    /**
     * @return Hash
     */
    public function getHash();
}
