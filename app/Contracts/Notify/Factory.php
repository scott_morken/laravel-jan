<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 2:32 PM
 */

namespace App\Contracts\Notify;

interface Factory
{

    /**
     * @param $name
     * @return \App\Contracts\Notify\Notifier
     */
    public function make($name);
}
