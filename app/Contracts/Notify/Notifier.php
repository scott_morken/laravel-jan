<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 11:16 AM
 */

namespace App\Contracts\Notify;

use Illuminate\Contracts\Logging\Log;

interface Notifier
{

    /**
     * @param mixed $to
     * @param mixed $payload
     * @return bool
     */
    public function send($to, $payload);

    /**
     * @return mixed
     */
    public function getBackend();

    /**
     * @return Log
     */
    public function getLogger();
}
