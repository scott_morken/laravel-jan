<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/10/17
 * Time: 8:22 AM
 */

namespace App\Contracts\Storage;

interface File
{

    public function cleanup();
}
