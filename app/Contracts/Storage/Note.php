<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:39 AM
 */

namespace App\Contracts\Storage;

interface Note
{

    /**
     * @param $key
     * @param bool $increment
     * @return \App\Contracts\Models\Note
     */
    public function getByKey($key, $increment = true);

    /**
     * @return int
     */
    public function removeExpired();

    /**
     * @return int
     */
    public function removeViewed();
}
