<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/10/17
 * Time: 7:58 AM
 */

namespace App\Contracts\Token;

use Illuminate\Contracts\Session\Session;

interface Store
{

    /**
     * @param $id
     * @param int $length
     * @return string
     */
    public function generate($id, $length = 16);

    /**
     * @param $id
     * @return string
     */
    public function retrieve($id);

    /**
     * @param $id
     * @param $token
     * @return bool
     */
    public function verify($id, $token);

    /**
     * @return void
     */
    public function reset();

    /**
     * @return \App\Contracts\Token\Token
     */
    public function getToken();

    /**
     * @return Session
     */
    public function getSession();
}
