<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:42 AM
 */

namespace App\Contracts\Token;

interface Token
{

    /**
     * @return int
     */
    public function getLength();

    /**
     * @param null $length
     * @return string
     */
    public function generate($length = null);
}
