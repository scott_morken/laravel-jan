<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Smorken\ControllerTraited\BaseController;
use Smorken\ControllerTraited\TraitedController;

abstract class Controller extends BaseController
{
    protected $master = 'layouts.master';

    use DispatchesJobs;
}
