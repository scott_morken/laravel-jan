<?php namespace App\Http\Controllers;

use App\Contracts\Key\Key;
use App\Contracts\Note\Builder;
use App\Contracts\Note\FileRetriever;
use App\Contracts\Note\Retriever;
use App\Contracts\Storage\Expire;
use App\Contracts\Storage\Notification;
use App\Contracts\Token\Store;
use App\Http\Requests\LookupForm;
use App\Http\Requests\NoteForm;
use App\Services\Notifiers\NotifyException;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('reset.tokens')->except('file');
        parent::__construct();
    }

    public function index()
    {
        return view('home');
    }

    public function create(Expire $expireProvider, Key $keyProvider, Notification $notificationProvider)
    {
        $expires = $expireProvider->all();
        $key = $keyProvider->get();
        $notifiers = $notificationProvider->all();
        return view('create')
            ->with('key', $key)
            ->with('expires', $expires)
            ->with('notifiers', $notifiers);
    }

    public function saveNote(NoteForm $r, Builder $builder)
    {
        $data = $r->only('key', 'shared', 'views_allowed', 'expire_at', 'note', 'files');
        try {
            if (!$builder->build($data)) {
                return redirect()->action($this->getRoute('create'))
                                 ->withErrors($builder->getErrors())
                                 ->withInput();
            }
        } catch (NotifyException $e) {
            session()->flash('danger', $e->getMessage());
        }
        session()->flash('success', 'Your note has been created.');
        return redirect()->action($this->getRoute('index'));
    }

    public function retrieve(LookupForm $r, Retriever $retriever, Store $tokenStore)
    {
        $note = $retriever->get($r->key, $r->shared);
        if (is_null($note)) {
            session()->flash(
                'danger',
                'The requested note could not be found. It may have expired, reached its max views or does not exist.'
            );
            return redirect()->action($this->getRoute('index'));
        }
        $token = $tokenStore->generate($note->id);
        return view('view')->with('note', $note)->with('token', $token);
    }

    public function file(FileRetriever $retriever, Store $tokenStore, $id, $token)
    {
        $file = $retriever->get($id);
        if (is_null($file)) {
            session()->flash('danger', 'The requested file could not be found.');
            return redirect()->action($this->getRoute('index'));
        }
        $note_id = $file->note_id;
        if (!$tokenStore->verify($note_id, $token)) {
            session()->flash('danger', 'The request token was invalid for the file.');
            return redirect()->action($this->getRoute('index'));
        }
        $headers = [
            'Content-Type'        => $file->mime,
            'Content-Disposition' => sprintf('attachment; filename="%s"', $file->filename),
            'Content-Length'      => $file->size,
        ];
        return response($file->data, 200, $headers);
    }

    public function genKey(Request $r, Key $keyProvider)
    {
        $key = $keyProvider->get();
        if (!$r->ajax()) {
            abort(403, "Not XHR.");
        }
        return response()->json(['key' => $key]);
    }
}
