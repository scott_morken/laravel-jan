<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/10/17
 * Time: 8:00 AM
 */

namespace App\Http\Middleware;

use App\Contracts\Token\Store;
use Closure;

class ResetTokens
{

    /**
     * @var Store
     */
    protected $tokenStore;

    public function __construct(Store $tokenStore)
    {
        $this->tokenStore = $tokenStore;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->tokenStore->reset();
        return $next($request);
    }
}
