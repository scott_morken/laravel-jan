<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 4:48 PM
 */

namespace App\Http\Requests;

use Smorken\Sanitizer\Contracts\Sanitize;

class LookupForm extends Request
{

    public function validator(Sanitize $s, $validation_factory)
    {
        $this->sanitize($s);
        $input = $this->all();
        $rules = $this->rules();
        if (app()->environment() !== 'testing' && env('NOCAPTCHA_SITEKEY')) {
            $rules['g-recaptcha-response'] = 'required|captcha';
        }
        return $validation_factory->make(
            $input,
            $rules,
            $this->messages(),
            $this->attributes()
        );
    }

    public function rules()
    {
        return [
            'key'    => 'required|integer',
            'shared' => 'required',
        ];
    }

    /**
     * @param array $input
     * @param Sanitize $s
     * @return array
     */
    protected function doSanitize($input, Sanitize $s)
    {
        $input['key'] = $s->int(array_get($input, 'key', null));
        $input['shared'] = $s->string(array_get($input, 'shared', null));
        return $input;
    }
}
