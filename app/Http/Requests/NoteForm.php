<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 4:02 PM
 */

namespace App\Http\Requests;

use App\Contracts\Storage\Expire;
use App\Contracts\Storage\Notification;
use Illuminate\Http\UploadedFile;
use Smorken\Sanitizer\Contracts\Sanitize;

class NoteForm extends Request
{

    public function validator(Sanitize $s, Notification $notification, Expire $expire, $validation_factory)
    {
        $this->sanitize($s);
        $input = $this->all();
        $rules = $this->baseRules($notification, $expire);
        $rules = $this->modRules($rules, $input);
        if (app()->environment() !== 'testing' && env('NOCAPTCHA_SITEKEY')) {
            $rules['g-recaptcha-response'] = 'required|captcha';
        }
        return $validation_factory->make(
            $input,
            $rules,
            $this->messages(),
            $this->attributes()
        );
    }

    /**
     * @param array $input
     * @param Sanitize $s
     * @return array
     */
    protected function doSanitize($input, Sanitize $s)
    {
        $input['key']['value'] = $s->int(array_get($input, 'key.value', null));
        $input['shared']['value'] = $s->string(array_get($input, 'shared.value', null));
        $input['key']['notification_id'] = $s->alpha(array_get($input, 'key.notification_id', null));
        $input['shared']['notification_id'] = $s->alpha(array_get($input, 'shared.notification_id', null));
        $input['key']['to'] = $s->string(array_get($input, 'key.to', null));
        $input['shared']['to'] = $s->string(array_get($input, 'shared.to', null));
        $input['views_allowed'] = $s->int(array_get($input, 'views_allowed', 1));
        $input['expire_at'] = $s->alphaNum(array_get($input, 'expire_at', null));
        $input['note'] = $s->string(array_get($input, 'note', null));
        return $input;
    }

    protected function modRules($rules, $input)
    {
        $rules['key.to'] = $this->getToRule($input, 'key');
        $rules['shared.to'] = $this->getToRule($input, 'shared');
        if (!$rules['key.to']) {
            unset($rules['key.to']);
        }
        if (!$rules['shared.to']) {
            unset($rules['shared.to']);
        }
        $rules = $this->addFileRules($rules, $input);
        return $rules;
    }

    protected function addFileRules($rules, $input)
    {
        $files = array_get($input, 'files', []);
        $max = UploadedFile::getMaxFilesize();
        foreach ($files as $i => $file) {
            $rules['files.' . $i] = 'max:' . $max;
        }
        return $rules;
    }

    protected function addInRule($collection)
    {
        $ids = $collection->pluck('id')->toArray();
        return 'in:' . implode(',', $ids);
    }

    protected function getToRule($input, $type)
    {
        $k = $type . '.' . 'notification_id';
        $val = array_get($input, $k);
        if ($val) {
            if ($val === 'E') {
                return 'required|email';
            } elseif ($val === 'S') {
                return 'required';
            }
        }
    }

    protected function baseRules(Notification $notification, Expire $expire)
    {
        $notifications = $notification->all();
        $expires = $expire->all();
        return [
            'key.value'              => 'required|integer',
            'key.notification_id'    => 'required|' . $this->addInRule($notifications),
            'key.to'                 => '',
            'shared.value'           => 'required|min:8',
            'shared.notification_id' => 'required|' . $this->addInRule($notifications),
            'shared.to'              => '',
            'views_allowed'          => 'required|integer|min:1|max:255',
            'expire_at'              => 'required|' . $this->addInRule($expires),
            'note'                   => 'required|max:4096',
        ];
    }
}
