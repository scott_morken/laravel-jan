<?php namespace App\Http\Requests;

class Request extends \Smorken\Ext\Http\Requests\Request
{

//    public function rules(Sanitize $s, ProviderContract $provider)
//    {
//        return $this->sanitizeAndGetRules($s, $provider);
//    }
//      OR
//    public function validator(Sanitize $s, AccessControlRequest $provider, $validation_factory)
//    {
//        $this->sanitize($s);
//        return $validation_factory->make($this->all(), $provider->getModel()->rules(), $this->messages(), $this->attributes());
//    }

    public function messages()
    {
        $messages = parent::messages();
        $messages['g-recaptcha-response.required'] = 'Please verify that you are not a robot.';
        return $messages;
    }
}
