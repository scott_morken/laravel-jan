<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:28 AM
 */

namespace App\Models\Config;

class Expire extends Base implements \App\Contracts\Models\Expire
{

    protected $config_key = 'models.expire';

    public function conversions()
    {
        return [
            'default' => [
                'id'          => 'id',
                'descr'       => 'descr',
                'time_string' => 'time_string',
                'weight'      => 'weight',
            ],
        ];
    }

    public function scopeOrderDefault($q)
    {
        return $q->sortBy('weight');
    }
}
