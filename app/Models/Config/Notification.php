<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:13 AM
 */

namespace App\Models\Config;

class Notification extends Base implements \App\Contracts\Models\Notification
{

    protected $config_key = 'models.notification';

    public function conversions()
    {
        return [
            'default' => [
                'id'        => 'id',
                'descr'     => 'descr',
                'handler'   => 'handler',
                'vector_id' => 'vector_id',
            ],
        ];
    }
}
