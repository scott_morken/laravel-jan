<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:25 AM
 */

namespace App\Models\Config;

class Vector extends Base implements \App\Contracts\Models\Vector
{

    protected $config_key = 'models.vector';

    public function conversions()
    {
        return [
            'default' => [
                'id'        => 'id',
                'descr'     => 'descr',
                'handler'   => 'handler',
                'vector_id' => 'vector_id',
            ],
        ];
    }
}
