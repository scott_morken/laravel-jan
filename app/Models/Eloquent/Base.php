<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/27/15
 * Time: 9:17 AM
 */

namespace App\Models\Eloquent;

use Smorken\Model\Eloquent\Model;
use Smorken\Storage\Contracts\Models\Crud;

abstract class Base extends Model implements Crud
{

    use \Smorken\Storage\Models\Traits\Model;
}
