<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/10/17
 * Time: 8:38 AM
 */

namespace App\Models\Eloquent;

class File extends Base implements \App\Contracts\Models\File
{

    protected $fillable = [
        'note_id',
        'filename',
        'mime',
        'size',
        'data',
    ];

    protected $rules = [
        'note_id'  => 'required|int',
        'filename' => 'required',
        'mime'     => 'required',
        'size'     => 'required|int',
        'data'     => 'required',
    ];

    public function note()
    {
        return $this->belongsTo(Note::class);
    }

    public function scopeMissingNote($q)
    {
        return $q->doesntHave('note');
    }
}
