<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 4:03 PM
 */

namespace App\Models\Eloquent;

class Note extends Base implements \App\Contracts\Models\Note
{

    protected $fillable = ['expire_at', 'key', 'data', 'views', 'views_allowed'];

    protected $rules = [
        'expire_at'     => 'required|date',
        'key'           => 'required',
        'data'          => 'required',
        'views'         => 'integer',
        'views_allowed' => 'required|integer',
    ];

    public function scopeKeyIs($q, $key)
    {
        return $q->where('key', '=', $key);
    }

    public function scopeNotExpired($q)
    {
        return $q->where('expire_at', '>', date('Y-m-d H:i:s'));
    }

    public function scopeExpired($q)
    {
        return $q->where('expire_at', '<', date('Y-m-d H:i:s'));
    }

    public function scopeViewed($q)
    {
        return $q->where('views', '>=', \DB::raw($this->getTable() . '.views_allowed'));
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }
}
