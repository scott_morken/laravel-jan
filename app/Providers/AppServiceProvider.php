<?php namespace App\Providers;

use App\Contracts\Key\Hash;
use App\Contracts\Key\Key;
use App\Contracts\Storage\Note;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register()
    {
        $this->bindKeyProvider();
        $this->bindHashProvider();
    }

    protected function bindHashProvider()
    {
        $this->app->bind(
            Hash::class,
            function ($app) {
                $np = $app[Note::class];
                return new \App\Services\Key\Hash($np, config('key.key_salt'));
            }
        );
    }

    protected function bindKeyProvider()
    {
        $this->app->bind(
            Key::class,
            function ($app) {
                return new \App\Services\Key\Key(config('key.key_length', 4));
            }
        );
    }
}
