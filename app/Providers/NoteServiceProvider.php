<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 2:17 PM
 */

namespace App\Providers;

use App\Contracts\Key\Hash;
use App\Contracts\Note\Builder;
use App\Contracts\Note\FileRetriever;
use App\Contracts\Note\KeySharedBuilder;
use App\Contracts\Note\Retriever;
use App\Contracts\Notify\Factory;
use App\Contracts\Storage\Expire;
use App\Contracts\Storage\File;
use App\Contracts\Storage\Note;
use App\Contracts\Storage\Notification;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;

class NoteServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->bindKeySharedBuilder();
        $this->bindBuilder();
        $this->bindRetrievers();
    }

    protected function bindBuilder()
    {
        $this->app->bind(
            Builder::class,
            function ($app) {
                $ks = $app[KeySharedBuilder::class];
                $np = $app[Note::class];
                $ep = $app[Expire::class];
                $fp = $app[File::class];
                $enc = $app[Encrypter::class];
                $l = $app[Log::class];
                return new \App\Services\Note\Builder($ks, $np, $ep, $enc, $fp, $l);
            }
        );
    }

    protected function bindKeySharedBuilder()
    {
        $this->app->bind(
            KeySharedBuilder::class,
            function ($app) {
                $f = $app[Factory::class];
                $h = $app[Hash::class];
                $np = $app[Notification::class];
                $url = $app[UrlGenerator::class];
                return new \App\Services\Note\KeySharedBuilder($f, $h, $np, $url);
            }
        );
    }

    protected function bindRetrievers()
    {
        $this->app->bind(
            Retriever::class,
            function ($app) {
                $e = $app[Encrypter::class];
                $np = $app[Note::class];
                $h = $app[Hash::class];
                return new \App\Services\Note\Retriever($e, $np, $h);
            }
        );
        $this->app->bind(
            FileRetriever::class,
            function ($app) {
                $e = $app[Encrypter::class];
                $fp = $app[File::class];
                return new \App\Services\Note\FileRetriever($e, $fp);
            }
        );
    }
}
