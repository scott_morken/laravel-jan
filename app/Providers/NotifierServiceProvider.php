<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 11:20 AM
 */

namespace App\Providers;

use App\Contracts\Notify\Email;
use App\Contracts\Notify\Factory;
use App\Contracts\Notify\Offline;
use App\Contracts\Notify\Sms;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\ServiceProvider;
use Twilio\Rest\Client;

class NotifierServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->bindEmail();
        $this->bindOffline();
        $this->bindSms();
        $this->app->bind(
            Factory::class,
            function ($app) {
                $notifiers = [
                    Email::class   => $app[Email::class],
                    Offline::class => $app[Offline::class],
                    Sms::class     => $app[Sms::class],
                ];
                return new \App\Services\Notifiers\Factory($notifiers);
            }
        );
    }

    protected function bindEmail()
    {
        $this->app->bind(
            Email::class,
            function ($app) {
                $mailer = $app['mailer'];
                $l = $app[Log::class];
                return new \App\Services\Notifiers\Email($mailer, $l, config('notifiers.email.settings', []));
            }
        );
    }

    protected function bindOffline()
    {
        $this->app->bind(
            Offline::class,
            function ($app) {
                $l = $app[Log::class];
                return new \App\Services\Notifiers\Offline(null, $l, []);
            }
        );
    }

    protected function bindSms()
    {
        $this->app->bind(
            Sms::class,
            function ($app) {
                $sid = env('TWILIO_ACCOUNT_SID');
                $authtoken = env('TWILIO_AUTH_TOKEN');
                $client = null;
                if ($sid && $authtoken) {
                    $client = new Client(env('TWILIO_ACCOUNT_SID'), env('TWILIO_AUTH_TOKEN'));
                }
                $l = $app[Log::class];
                return new \App\Services\Notifiers\TwilioSms($client, $l, config('notifiers.sms.settings', []));
            }
        );
    }
}
