<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:49 AM
 */

namespace App\Providers;

use App\Contracts\Token\Store;
use App\Contracts\Token\Token;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\ServiceProvider;

class TokenServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            Token::class,
            function ($app) {
                $length = config('app.token_length', 32);
                return new \App\Services\Token\Token($length);
            }
        );

        $this->app->bind(
            Store::class,
            function ($app) {
                $t = $app[Token::class];
                $s = $app[Session::class];
                return new \App\Services\Token\Store($t, $s);
            }
        );
    }
}
