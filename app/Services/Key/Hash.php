<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 10:08 AM
 */

namespace App\Services\Key;

use App\Contracts\Storage\Note;
use Illuminate\Contracts\Hashing\Hasher;

class Hash implements \App\Contracts\Key\Hash
{

    /**
     * @var Note
     */
    protected $provider;

    /**
     * @var string
     */
    protected $salt;

    protected $type = 'sha256';

    public function __construct(Note $provider, $salt, $type = null)
    {
        if ($type) {
            $this->type = $type;
        }
        $this->provider = $provider;
        $this->setSalt($salt);
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    protected function setSalt($salt)
    {
        if (!$salt || strlen($salt) < 8) {
            throw new KeyException('Salt must be at least 8 characters long.');
        }
        $this->salt = $salt;
    }

    /**
     * @param $key
     * @param $shared
     * @param bool $throw
     * @return string
     * @throws KeyException
     */
    public function create($key, $shared, $throw = true)
    {
        $hashed = $this->hash($key, $shared, $this->getSalt());
        if ($throw && $this->check($hashed)) {
            throw new KeyException('Please enter a different shared secret or key.');
        }
        return $hashed;
    }

    protected function hash($key, $shared, $salt)
    {
        return hash($this->type, $key . $shared . $salt);
    }

    /**
     * @return Note
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param $key
     * @return bool
     */
    public function check($key)
    {
        return $this->getProvider()->getByKey($key, false) instanceof \App\Contracts\Models\Note;
    }
}
