<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 10:08 AM
 */

namespace App\Services\Key;

class Key implements \App\Contracts\Key\Key
{

    /**
     * @var int
     */
    protected $length;

    public function __construct($length = 4)
    {
        $this->setLength($length);
    }

    protected function setLength($length)
    {
        $length = (int)$length;
        if ($length < 2) {
            $length = 2;
        }
        if ($length > 16) {
            $length = 16;
        }
        $this->length = $length;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param null $length
     * @return int
     */
    public function get($length = null)
    {
        if (!is_null($length)) {
            $this->setLength($length);
        }
        $length = $this->getLength();
        $max = $this->getMax($length);
        return mt_rand(1, $max);
    }

    protected function getMax($length)
    {
        return (int)str_repeat('9', $length);
    }
}
