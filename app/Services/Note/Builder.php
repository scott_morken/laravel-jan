<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 12:16 PM
 */

namespace App\Services\Note;

use App\Contracts\Storage\Expire;
use App\Contracts\Storage\File;
use App\Contracts\Storage\Note;
use App\Services\Key\KeyException;
use App\Services\Notifiers\NotifyException;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\UploadedFile;

class Builder implements \App\Contracts\Note\Builder
{

    /**
     * @var Note
     */
    protected $provider;

    /**
     * @var KeySharedBuilder
     */
    protected $ksBuilder;

    /**
     * @var Expire
     */
    protected $expireProvider;

    /**
     * @var Encrypter
     */
    protected $encrypter;

    /**
     * @var File
     */
    protected $fileProvider;

    /**
     * @var Log
     */
    protected $logger;

    protected $errors = [];

    public function __construct(
        \App\Contracts\Note\KeySharedBuilder $ksBuilder,
        Note $provider,
        Expire $expireProvider,
        Encrypter $encrypter,
        File $fileProvider,
        Log $logger
    ) {
        $this->ksBuilder = $ksBuilder;
        $this->provider = $provider;
        $this->expireProvider = $expireProvider;
        $this->encrypter = $encrypter;
        $this->fileProvider = $fileProvider;
        $this->logger = $logger;
    }

    /**
     * ['key' => [
     *      'value' => ?
     *      'notification_id' => ?
     *      'to' => ?
     *   ],
     *   'shared' => [
     *      'value' => ?
     *      'notification_id' => ?
     *      'to' => ?
     *   ],
     *   'views_allowed' => ?,
     *   'expire_at' => ?,
     *   'note' => ?,
     *   'files' => [...],
     * ]
     * @param array $data
     * @return bool
     * @throws NotifyException
     */
    public function build(array $data)
    {
        try {
            $this->getKeySharedBuilder()->build($data);
            $item = [
                'key'           => $this->getKeySharedBuilder()->getHash(),
                'data'          => $this->getEncryptedNote($data),
                'expire_at'     => $this->getExpiryDate($data),
                'views_allowed' => $this->getViewsAllowed($data),
                'views'         => 0,
            ];
            $note = $this->getProvider()->create($item);
            if (!$note) {
                $this->errors[] = 'There was an error creating a note.';
            } else {
                $files = array_get($data, 'files', []);
                if (!$this->addFiles($note, $files)) {
                    $note->delete();
                    $this->errors[] = 'There was an error saving one or more files.';
                }
                $this->parseNotifyResult($this->getKeySharedBuilder()->notify());
            }
        } catch (BuilderException $be) {
            $this->logger->error($be);
            $this->errors[] = $be->getMessage();
        } catch (KeyException $ke) {
            $this->logger->error($ke);
            $this->errors[] = 'There was an error generating a key, please try again.';
        }
        return !$this->hasErrors();
    }

    protected function parseNotifyResult($result)
    {
        $key = $result['key'];
        $shared = $result['shared'];
        if ($key && $shared) {
            return true;
        }
        $types = [];
        if (!$key) {
            $types[] = 'key';
        }
        if (!$shared) {
            $types[] = 'shared';
        }
        $msg = sprintf(
            'There was an error sending notifications (%s), you will need to do so manually.',
            implode(', ', $types)
        );
        throw new NotifyException(implode(', ', $types), $msg);
    }

    /**
     * @return \App\Contracts\Note\KeySharedBuilder
     */
    public function getKeySharedBuilder()
    {
        return $this->ksBuilder;
    }

    /**
     * @return Note
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @return Expire
     */
    public function getExpireProvider()
    {
        return $this->expireProvider;
    }

    /**
     * @return File
     */
    public function getFileProvider()
    {
        return $this->fileProvider;
    }

    /**
     * @return Encrypter
     */
    public function getEncryptor()
    {
        return $this->encrypter;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return count($this->getErrors()) > 0;
    }

    /**
     * @param \App\Contracts\Models\Note $note
     * @param UploadedFile[]|null $files
     * @return bool
     */
    protected function addFiles(\App\Contracts\Models\Note $note, $files)
    {
        if (!$files) {
            return true;
        }
        $error = false;
        foreach ($files as $file) {
            if ($file->isValid()) {
                $contents = file_get_contents($file->path());
                $enc = $this->getEncryptor()->encrypt($contents);
                $data = [
                    'note_id'  => $note->id,
                    'mime'     => $file->getMimeType(),
                    'size'     => $file->getSize(),
                    'filename' => $file->getClientOriginalName(),
                    'data'     => $enc,
                ];
                if (!$this->getFileProvider()->create($data)) {
                    $error = true;
                }
            } else {
                $error = true;
            }
        }
        return !$error;
    }

    protected function getViewsAllowed(array $data)
    {
        $allowed = array_get($data, 'views_allowed', 1);
        return $allowed;
    }

    protected function getEncryptedNote(array $data)
    {
        $str = array_get($data, 'note', null);
        return $this->getEncryptor()->encrypt($str);
    }

    protected function getExpiryDate(array $data)
    {
        $str = array_get($data, 'expire_at', null);
        $m = $this->getExpireProvider()->find($str);
        if ($m) {
            return date('Y-m-d H:i:s', strtotime($m->time_string));
        }
        return date('Y-m-d H:i:s', strtotime('+1 hour'));
    }
}
