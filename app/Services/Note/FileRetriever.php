<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/10/17
 * Time: 10:24 AM
 */

namespace App\Services\Note;

use App\Contracts\Models\File;
use Illuminate\Contracts\Encryption\Encrypter;

class FileRetriever implements \App\Contracts\Note\FileRetriever
{

    /**
     * @var Encrypter
     */
    protected $encrypter;

    /**
     * @var \App\Contracts\Storage\File
     */
    protected $provider;

    public function __construct(Encrypter $encrypter, \App\Contracts\Storage\File $provider)
    {
        $this->encrypter = $encrypter;
        $this->provider = $provider;
    }

    /**
     * @param $id
     * @return File
     */
    public function get($id)
    {
        $m = $this->getProvider()->find($id);
        if ($m) {
            $m->data = $this->getEncrypter()->decrypt($m->data);
        }
        return $m;
    }

    /**
     * @return Encrypter
     */
    public function getEncrypter()
    {
        return $this->encrypter;
    }

    /**
     * @return \App\Contracts\Storage\File
     */
    public function getProvider()
    {
        return $this->provider;
    }
}
