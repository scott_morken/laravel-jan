<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 12:49 PM
 */

namespace App\Services\Note;

use App\Contracts\Key\Hash;
use App\Contracts\Notify\Factory;
use App\Contracts\Notify\Notifier;
use App\Contracts\Storage\Notification;
use Illuminate\Contracts\Routing\UrlGenerator;

class KeySharedBuilder implements \App\Contracts\Note\KeySharedBuilder
{

    /**
     * @var Factory
     */
    protected $factory;

    /**
     * @var Hash
     */
    protected $hasher;

    /**
     * @var Notification
     */
    protected $notificationProvider;

    /**
     * @var UrlGenerator
     */
    protected $url;

    protected $notification_ids = [];

    protected $data = [];

    public function __construct(
        Factory $factory,
        Hash $hasher,
        Notification $notification,
        UrlGenerator $url
    ) {
        $this->factory = $factory;
        $this->hasher = $hasher;
        $this->notificationProvider = $notification;
        $this->url = $url;
    }

    public function build(
        array $data
    ) {
        $this->data['key'] = $this->prepareNotifierData('key', $data);
        $this->data['shared'] = $this->prepareNotifierData('shared', $data);
        return $this->verify();
    }

    public function getValue(
        $type
    ) {
        return array_get($this->data, $type . '.value', null);
    }

    /**
     * @param $type
     * @return Notifier|null
     */
    public function getNotifier(
        $type
    ) {
        return array_get($this->data, $type . '.notifier', null);
    }

    public function getTo(
        $type
    ) {
        return array_get($this->data, $type . '.to', null);
    }

    protected function verify()
    {
        if ($this->getValue('key') && $this->getValue('shared') && $this->getNotifier('key') &&
            $this->getNotifier('shared')
        ) {
            return true;
        } else {
            throw new BuilderException('Missing required component(s).');
        }
    }

    /**
     * @return string
     */
    public function getHash()
    {
        $this->verify();
        return $this->getHasher()->create($this->getValue('key'), $this->getValue('shared'));
    }

    public function notify()
    {
        $this->verify();
        $key = $this->notifyWithMessage('key', 'Key: %s');
        $shared = $this->notifyWithMessage('shared', 'Shared Knowledge: %s');
        return ['key' => $key, 'shared' => $shared];
    }

    protected function notifyWithMessage(
        $type,
        $msg
    ) {
        $value = $this->getValue($type);
        $notifier = $this->getNotifier($type);
        $to = $this->getTo($type);
        return $notifier->send($to, ['msg' => sprintf($msg, $value), 'url' => $this->getUrl()->to('')]);
    }

    /**
     * @return UrlGenerator
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return Notification
     */
    public function getNotificationProvider()
    {
        return $this->notificationProvider;
    }

    /**
     * @return Hash
     */
    public function getHasher()
    {
        return $this->hasher;
    }

    /**
     * @return Factory
     */
    public function getNotifierFactory()
    {
        return $this->factory;
    }

    /**
     * @param $id
     * @return \App\Contracts\Models\Notification|null
     * @throws BuilderException
     */
    protected function getNotificationById(
        $id
    ) {
        if (!isset($this->notification_ids[$id])) {
            $n = $this->getNotificationProvider()->find($id);
            if ($n) {
                $this->notification_ids[$id] = $n;
            } else {
                throw new BuilderException('Unable to find notification.');
            }
        }
        return array_get($this->notification_ids, $id, null);
    }

    protected function prepareNotifierData(
        $type,
        array $data
    ) {
        $prepared = [];
        $notification_id = array_get($data, $type . '.notification_id', null);
        if ($notification_id) {
            $notification = $this->getNotificationById($notification_id);
            $notifier = $this->createNotifier($notification);
            if ($notifier) {
                $prepared['value'] = array_get($data, $type . '.value');
                $prepared['to'] = array_get($data, $type . '.to');
                $prepared['notifier'] = $notifier;
            }
        } else {
            throw new BuilderException('No notification provided.');
        }
        return $prepared;
    }

    protected function createNotifier(
        \App\Contracts\Models\Notification $n = null
    ) {
        if ($n) {
            $notifier = $this->getNotifierFactory()->make($n->handler);
            if ($notifier) {
                return $notifier;
            }
            throw new BuilderException('Unable to create notifier.');
        }
        return null;
    }
}
