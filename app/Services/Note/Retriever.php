<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 2:09 PM
 */

namespace App\Services\Note;

use App\Contracts\Key\Hash;
use App\Contracts\Storage\Note;
use Illuminate\Contracts\Encryption\Encrypter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Retriever implements \App\Contracts\Note\Retriever
{

    /**
     * @var Encrypter
     */
    protected $encrypter;

    /**
     * @var Note
     */
    protected $provider;

    /**
     * @var Hash
     */
    protected $hasher;

    public function __construct(Encrypter $encrypter, Note $provider, Hash $hasher)
    {
        $this->encrypter = $encrypter;
        $this->provider = $provider;
        $this->hasher = $hasher;
    }

    /**
     * @param $key
     * @param $shared
     * @return string
     */
    public function get($key, $shared)
    {
        $hash = $this->getHash()->create($key, $shared, false);
        $m = $this->getProvider()->getByKey($hash);
        if ($m) {
            $m->data = $this->getEncrypter()->decrypt($m->data);
            return $m;
        }
        return null;
    }

    /**
     * @return Encrypter
     */
    public function getEncrypter()
    {
        return $this->encrypter;
    }

    /**
     * @return Note
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @return Hash
     */
    public function getHash()
    {
        return $this->hasher;
    }
}
