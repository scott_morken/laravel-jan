<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 12:30 PM
 */

namespace App\Services\Notifiers;

use Illuminate\Contracts\Logging\Log;

abstract class Base
{

    protected $backend;

    protected $settings = [];

    /**
     * @var Log
     */
    protected $logger;

    public function __construct($backend, Log $logger, $settings = [])
    {
        $this->logger = $logger;
        $this->backend = $backend;
        $this->settings = $settings;
    }

    public function send($to, $payload)
    {
        return $this->sendViaBackend($to, $payload);
    }

    abstract protected function sendViaBackend($to, $payload);

    protected function getSetting($key, $default = null)
    {
        return array_get($this->settings, $key, $default);
    }

    public function getBackend()
    {
        return $this->backend;
    }

    /**
     * @return Log
     */
    public function getLogger()
    {
        return $this->logger;
    }
}
