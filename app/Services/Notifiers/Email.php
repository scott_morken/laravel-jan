<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 12:33 PM
 */

namespace App\Services\Notifiers;

use App\Mail\Notify;

class Email extends Base implements \App\Contracts\Notify\Email
{

    protected function sendViaBackend($to, $payload)
    {
        try {
            $subject = $this->getSetting('subject', 'Just A Note');
            $view = $this->getSetting('view', 'notifier.email');
            $n = new Notify($payload);
            $n->subject($subject);
            $n->view($view);
            $this->backend->to($to)->send($n);
            if (count($this->backend->failures()) === 0) {
                return true;
            }
        } catch (\Exception $e) {
            $this->getLogger()->error($e);
        }
        return false;
    }
}
