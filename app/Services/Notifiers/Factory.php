<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 11:17 AM
 */

namespace App\Services\Notifiers;

use App\Contracts\Notify\Notifier;

class Factory implements \App\Contracts\Notify\Factory
{

    protected $notifiers = [];

    /**
     * Factory constructor.
     * @param array $notifiers
     */
    public function __construct(array $notifiers)
    {
        $this->notifiers = $notifiers;
    }

    public function make($name)
    {
        if (isset($this->notifiers[$name])) {
            return $this->create($name);
        }
        throw new \InvalidArgumentException("$name is not a valid notifier.");
    }

    protected function create($name)
    {
        if ($this->notifiers[$name] instanceof Notifier) {
            return $this->notifiers[$name];
        }
        return app($name);
    }
}
