<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/13/17
 * Time: 5:23 PM
 */

namespace App\Services\Notifiers;

use Throwable;

class NotifyException extends \Exception
{

    protected $type;

    public function __construct($type, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->type = $type;
        parent::__construct($message, $code, $previous);
    }

    public function getType()
    {
        return $this->type;
    }
}
