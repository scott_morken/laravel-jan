<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 1:14 PM
 */

namespace App\Services\Notifiers;

class Offline extends Base implements \App\Contracts\Notify\Offline
{

    protected function sendViaBackend($to, $payload)
    {
        return true;
    }
}
