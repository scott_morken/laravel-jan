<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 1:14 PM
 */

namespace App\Services\Notifiers;

use Twilio\Exceptions\TwilioException;

class TwilioSms extends Base implements \App\Contracts\Notify\Sms
{

    protected function sendViaBackend($to, $payload)
    {
        if ($this->getBackend()) {
            try {
                $message = $this->payloadToMessage($payload);
                $from = $this->getSetting('number', null);
                $this->getBackend()->messages->create(
                    $to,
                    [
                        'body' => $message,
                        'from' => $from,
                    ]
                );
                return true;
            } catch (TwilioException $e) {
                $this->getLogger()->error($e);
            }
        }
        return false;
    }

    protected function payloadToMessage($payload)
    {
        return sprintf("Just A Note\n%s\nURL: %s", array_get($payload, 'msg'), array_get($payload, 'url'));
    }
}
