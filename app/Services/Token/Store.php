<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/10/17
 * Time: 7:52 AM
 */

namespace App\Services\Token;

use Illuminate\Contracts\Session\Session;

class Store implements \App\Contracts\Token\Store
{

    /**
     * @var \App\Contracts\Token\Token
     */
    protected $token;

    /**
     * @var Session
     */
    protected $session;

    protected $session_key = 'jan_tokens';

    public function __construct(\App\Contracts\Token\Token $token, Session $session)
    {
        $this->token = $token;
        $this->session = $session;
    }

    /**
     * @param $id
     * @param int $length
     * @return string
     */
    public function generate($id, $length = 16)
    {
        $t = $this->getToken()->generate($length);
        $tokens = $this->getTokens();
        $tokens[$id] = $t;
        $this->getSession()->put($this->session_key, $tokens);
        return $t;
    }

    /**
     * @return void
     */
    public function reset()
    {
        $this->getSession()->forget($this->session_key);
        $this->getSession()->regenerate();
    }

    /**
     * @return \App\Contracts\Token\Token
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    protected function getTokens()
    {
        return $this->getSession()->get($this->session_key, []);
    }

    /**
     * @param $id
     * @return string
     */
    public function retrieve($id)
    {
        $tokens = $this->getTokens();
        return array_get($tokens, $id, null);
    }

    /**
     * @param $id
     * @param $token
     * @return bool
     */
    public function verify($id, $token)
    {
        $stored = $this->retrieve($id);
        return ($token && $stored === $token);
    }
}
