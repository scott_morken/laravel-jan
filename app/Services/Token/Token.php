<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:43 AM
 */

namespace App\Services\Token;

class Token implements \App\Contracts\Token\Token
{

    protected $length = 32;

    public function __construct($length = 32)
    {
        $this->setLength($length);
    }

    /**
     * @param null $length
     * @return string
     */
    public function generate($length = null)
    {
        if (!is_null($length)) {
            $this->setLength($length);
        }
        $length = $this->getLength();
        if (function_exists('random_bytes')) {
            return bin2hex(random_bytes($length));
        }
        if (function_exists('mcrypt_create_iv')) { // deprecated by random_bytes in 7.1.0+
            return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
        }
        if (function_exists('openssl_random_pseudo_bytes')) {
            return bin2hex(openssl_random_pseudo_bytes($length));
        }
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    protected function setLength($length)
    {
        $length = (int)$length;
        if ($length < 8) {
            $length = 8;
        }
        if ($length > 64) {
            throw new \OutOfBoundsException("$length is not a valid size.");
        }
        $this->length = $length;
    }
}
