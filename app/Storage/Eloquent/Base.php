<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 4/4/16
 * Time: 3:04 PM
 */

namespace App\Storage\Eloquent;

use Smorken\Storage\Contracts\Storage\Crud;
use Smorken\Storage\Contracts\Storage\Pageable;
use Smorken\Storage\Storage\Base\AbstractEloquent;

abstract class Base extends AbstractEloquent implements Crud, Pageable
{

}
