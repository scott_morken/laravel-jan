<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/10/17
 * Time: 8:37 AM
 */

namespace App\Storage\Eloquent;

class File extends Base implements \App\Contracts\Storage\File
{

    public function cleanup()
    {
        return $this->getModel()
            ->newQuery()
            ->missingNote()
            ->delete();
    }
}
