<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 1:50 PM
 */

namespace App\Storage\Eloquent;

class Note extends Base implements \App\Contracts\Storage\Note
{

    /**
     * @param $key
     * @param bool $increment
     * @return \App\Contracts\Models\Note
     */
    public function getByKey($key, $increment = true)
    {
        $m = $this->getModel()
                  ->newQuery()
                  ->keyIs($key)
                  ->notExpired()
                  ->first();
        if ($m && $increment) {
            if ((int)$m->views >= (int)$m->views_allowed) {
                return null;
            } else {
                $m->increment('views');
            }
        }
        return $m;
    }

    /**
     * @return int
     */
    public function removeExpired()
    {
        return $this->getModel()
                    ->newQuery()
                    ->expired()
                    ->delete();
    }

    /**
     * @return int
     */
    public function removeViewed()
    {
        return $this->getModel()
                    ->newQuery()
                    ->viewed()
                    ->delete();
    }
}
