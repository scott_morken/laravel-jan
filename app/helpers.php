<?php
if (!function_exists('select_list')) {
    /**
     * Create a select list based on $key => $value (defaults to (string)$model)
     * from $items
     * $prepend is in form of ['' => 'Select one']
     * @param array $items
     * @param string $key
     * @param null|string $value
     * @param array $prepend
     * @return array
     */
    function select_list($items, $key = 'id', $value = null, $prepend = [])
    {
        $get_value = function ($model, $item) {
            if ($item instanceof \Closure) {
                return $item($model);
            }
            return ($item ? $model->$item : (string)$model);
        };
        $sl = [];
        if ($prepend) {
            $sl = $prepend;
        }
        foreach ($items as $m) {
            $k = $get_value($m, $key);
            $v = $get_value($m, $value);
            if ($k && $v) {
                $sl[$k] = $v;
            }
        }
        return $sl;
    }
}
