<project name="name-of-project" default="build">
    <!--  By default, we assume all tools to be on the $PATH  -->
    <property name="toolsdir" value=""/>
    <property environment="env"/>
    <!--
     Uncomment the following when the tools are in ${basedir}/vendor/bin
    -->
    <!--
     <property name="toolsdir" value="${basedir}/vendor/bin/"/>
    -->
    <target name="build" depends="prepare,copy,npm,composer,gulp,storage-permissions,lint,phploc-ci,pdepend,phpmd-ci,phpcs-ci,phpcpd-ci,phpunit,phpdox,build-file,tarballs,zips" description=""/>
    <target name="build-parallel" depends="prepare,copy,npm,composer,gulp,storage-permissions,lint,tools-parallel,phpunit,phpdox,build-file,tarballs,zips" description=""/>
    <target name="tools-parallel" description="Run tools in parallel">
        <parallel threadCount="2">
            <sequential>
                <antcall target="pdepend"/>
                <antcall target="phpmd-ci"/>
            </sequential>
            <antcall target="phpcpd-ci"/>
            <antcall target="phpcs-ci"/>
            <antcall target="phploc-ci"/>
        </parallel>
    </target>
    <target name="clean" unless="clean.done" description="Cleanup build artifacts">
        <delete dir="${basedir}/build/api"/>
        <delete dir="${basedir}/build/coverage"/>
        <delete dir="${basedir}/build/logs"/>
        <delete dir="${basedir}/build/pdepend"/>
        <delete dir="${basedir}/build/phpdox"/>
        <property name="clean.done" value="true"/>
    </target>
    <target name="prepare" unless="prepare.done" depends="clean" description="Prepare for build">
        <mkdir dir="${basedir}/build/api"/>
        <mkdir dir="${basedir}/build/coverage"/>
        <mkdir dir="${basedir}/build/logs"/>
        <mkdir dir="${basedir}/build/pdepend"/>
        <mkdir dir="${basedir}/build/phpdox"/>
        <property name="prepare.done" value="true"/>
    </target>
    <target name="lint" unless="lint.done" description="Perform syntax check of sourcecode files">
        <apply executable="php" failonerror="true" taskname="lint">
            <arg value="-l"/>
            <fileset dir="${basedir}/app">
                <include name="**/*.php"/>
                <modified/>
            </fileset>
            <fileset dir="${basedir}/config">
                <include name="**/*.php"/>
                <modified/>
            </fileset>
            <fileset dir="${basedir}/resources">
                <include name="**/*.php"/>
                <modified/>
            </fileset>
            <fileset dir="${basedir}/tests">
                <include name="**/*.php"/>
                <modified/>
            </fileset>
        </apply>
        <property name="lint.done" value="true"/>
    </target>
    <target name="phploc" unless="phploc.done" description="Measure project size using PHPLOC and print human readable output. Intended for usage on the command line.">
        <exec executable="${toolsdir}phploc" taskname="phploc">
            <arg value="--count-tests"/>
            <arg path="${basedir}/app"/>
            <arg path="${basedir}/config"/>
            <arg path="${basedir}/resources"/>
            <arg path="${basedir}/tests"/>
        </exec>
        <property name="phploc.done" value="true"/>
    </target>
    <target name="phploc-ci" unless="phploc.done" depends="prepare" description="Measure project size using PHPLOC and log result in CSV and XML format. Intended for usage within a continuous integration environment.">
        <exec executable="${toolsdir}phploc" taskname="phploc">
            <arg value="--count-tests"/>
            <arg value="--log-csv"/>
            <arg path="${basedir}/build/logs/phploc.csv"/>
            <arg value="--log-xml"/>
            <arg path="${basedir}/build/logs/phploc.xml"/>
            <arg path="${basedir}/app"/>
            <arg path="${basedir}/config"/>
            <arg path="${basedir}/resources"/>
            <arg path="${basedir}/tests"/>
        </exec>
        <property name="phploc.done" value="true"/>
    </target>
    <target name="pdepend" unless="pdepend.done" depends="prepare" description="Calculate software metrics using PHP_Depend and log result in XML format. Intended for usage within a continuous integration environment.">
        <exec executable="${toolsdir}pdepend" taskname="pdepend">
            <arg value="--jdepend-xml=${basedir}/build/logs/jdepend.xml"/>
            <arg value="--jdepend-chart=${basedir}/build/pdepend/dependencies.svg"/>
            <arg value="--overview-pyramid=${basedir}/build/pdepend/overview-pyramid.svg"/>
            <arg path="${basedir}/app,${basedir}/config,${basedir}/resources"/>
        </exec>
        <property name="pdepend.done" value="true"/>
    </target>
    <target name="phpmd" unless="phpmd.done" description="Perform project mess detection using PHPMD and print human readable output. Intended for usage on the command line before committing.">
        <exec executable="${toolsdir}phpmd" taskname="phpmd">
            <arg path="${basedir}/app,${basedir}/config,${basedir}/resources"/>
            <arg value="text"/>
            <arg path="${basedir}/build/phpmd.xml"/>
        </exec>
        <property name="phpmd.done" value="true"/>
    </target>
    <target name="phpmd-ci" unless="phpmd.done" depends="prepare" description="Perform project mess detection using PHPMD and log result in XML format. Intended for usage within a continuous integration environment.">
        <exec executable="${toolsdir}phpmd" taskname="phpmd">
            <arg path="${basedir}/app,${basedir}/config,${basedir}/resources"/>
            <arg value="xml"/>
            <arg path="${basedir}/build/phpmd.xml"/>
            <arg value="--reportfile"/>
            <arg path="${basedir}/build/logs/pmd.xml"/>
        </exec>
        <property name="phpmd.done" value="true"/>
    </target>
    <target name="phpcs" unless="phpcs.done" description="Find coding standard violations using PHP_CodeSniffer and print human readable output. Intended for usage on the command line before committing.">
        <exec executable="${toolsdir}phpcs" taskname="phpcs">
            <arg value="--standard=${basedir}/build/phpcs.xml"/>
            <arg value="--extensions=php"/>
            <arg value="--ignore=autoload.php"/>
            <arg path="${basedir}/app"/>
            <arg path="${basedir}/config"/>
            <arg path="${basedir}/resources"/>
        </exec>
        <property name="phpcs.done" value="true"/>
    </target>
    <target name="phpcbf" description="Fix coding standard violations using PHP_CodeSniffer and print human readable output. Intended for usage on the command line before committing.">
        <exec executable="${toolsdir}phpcbf" taskname="phpcbf">
            <arg value="--standard=${basedir}/build/phpcs.xml"/>
            <arg value="--extensions=php"/>
            <arg value="--ignore=autoload.php"/>
            <arg path="${basedir}/app"/>
            <arg path="${basedir}/config.copy"/>
            <arg path="${basedir}/resources.copy"/>
        </exec>
    </target>
    <target name="phpcs-ci" unless="phpcs.done" depends="prepare" description="Find coding standard violations using PHP_CodeSniffer and log result in XML format. Intended for usage within a continuous integration environment.">
        <exec executable="${toolsdir}phpcs" output="/dev/null" taskname="phpcs">
            <arg value="--report=checkstyle"/>
            <arg value="--report-file=${basedir}/build/logs/checkstyle.xml"/>
            <arg value="--standard=${basedir}/build/phpcs.xml"/>
            <arg value="--extensions=php"/>
            <arg value="--ignore=autoload.php"/>
            <arg path="${basedir}/app"/>
            <arg path="${basedir}/config"/>
            <arg path="${basedir}/resources"/>
        </exec>
        <property name="phpcs.done" value="true"/>
    </target>
    <target name="phpcpd" unless="phpcpd.done" description="Find duplicate code using PHPCPD and print human readable output. Intended for usage on the command line before committing.">
        <exec executable="${toolsdir}phpcpd" taskname="phpcpd">
            <arg path="${basedir}/app"/>
        </exec>
        <property name="phpcpd.done" value="true"/>
    </target>
    <target name="phpcpd-ci" unless="phpcpd.done" depends="prepare" description="Find duplicate code using PHPCPD and log result in XML format. Intended for usage within a continuous integration environment.">
        <exec executable="${toolsdir}phpcpd" taskname="phpcpd">
            <arg value="--log-pmd"/>
            <arg path="${basedir}/build/logs/pmd-cpd.xml"/>
            <arg path="${basedir}/app"/>
            <arg path="${basedir}/config"/>
            <arg path="${basedir}/resources"/>
        </exec>
        <property name="phpcpd.done" value="true"/>
    </target>
    <target name="phpunit" unless="phpunit.done" depends="prepare" description="Run unit tests with PHPUnit">
        <exec executable="${toolsdir}phpunit" failonerror="true" taskname="phpunit">
            <arg value="--configuration"/>
            <arg path="${basedir}/phpunit.xml"/>
            <arg value="--coverage-clover"/>
            <arg path="${basedir}/build/logs/clover.xml"/>
            <arg value="--coverage-crap4j"/>
            <arg path="${basedir}/build/logs/crap4j.xml"/>
            <arg value="--log-junit"/>
            <arg path="${basedir}/build/logs/junit.xml"/>
        </exec>
        <property name="phpunit.done" value="true"/>
    </target>
    <target name="phpdox" unless="phpdox.done" depends="phploc-ci,phpcs-ci,phpmd-ci" description="Generate project documentation using phpDox">
        <exec executable="${toolsdir}phpdox" dir="${basedir}/build" taskname="phpdox"/>
        <property name="phpdox.done" value="true"/>
    </target>
    <target name="copy" unless="copy.done" description="Copy gitignored to live">
        <copy todir="${basedir}/config">
            <fileset dir="${basedir}/config.copy" />
        </copy>
        <copy todir="${basedir}/public">
            <fileset dir="${basedir}/public.copy" />
        </copy>
        <copy todir="${basedir}/resources">
            <fileset dir="${basedir}/resources.copy" />
        </copy>
        <copy file="${basedir}/public/.htaccess.copy" tofile="${basedir}/public/.htaccess"/>
        <copy file="${basedir}/public/favicon.ico.copy" tofile="${basedir}/public/favicon.ico"/>
        <copy file="${basedir}/public/robots.txt.copy" tofile="${basedir}/public/robots.txt"/>
        <copy file="${basedir}/.env.jenkins" tofile="${basedir}/.env"/>
        <property name="copy.done" value="true"/>
    </target>
    <target name="clean-composer" description="Cleanup composer artifacts">
        <echo>Cleaning out the composer artifacts</echo>
        <delete dir="${basedir}/vendor" />
        <delete file="${basedir}/composer.lock" />
    </target>
    <target name="composer" depends="composer-update" description="Install or update dependencies" />
    <target name="composer-update" description="Updating dependencies">
        <echo>Installing/Updating Composer dependencies</echo>
        <exec executable="composer" failonerror="true">
            <arg value="update" />
            <arg value="--no-scripts" />
            <arg value="--ignore-platform-reqs" />
        </exec>
    </target>
    <target name="npm" unless="npm.done" description="Installing npm dependencies">
        <exec executable="npm" failonerror="true">
            <arg value="install" />
        </exec>
        <property name="npm.done" value="true"/>
    </target>
    <target name="clean-npm" description="Cleanup node dependencies">
        <echo>Cleaning out the bower artifacts</echo>
        <delete dir="${basedir}/node_modules" />
    </target>
    <target name="gulp" description="Gulp assets" depends="copy,npm,composer">
        <exec executable="gulp" failonerror="true">
        </exec>
    </target>
    <target name="storage-permissions" description="Setting storage permissions">
        <echo>Setting storage, bootstrap, and vendor to 770</echo>
        <chmod file="${basedir}/storage/**" perm="770" type="dir" failonerror="false" />
        <chmod file="${basedir}/bootstrap/**" perm="770" type="dir" failonerror="false" />
        <chmod file="${basedir}/vendor/**" perm="770" type="dir" failonerror="false" />
    </target>
    <target name="deploy-script" description="Creates an example deployment script">
        <property name="scriptname" value="deploy.sh"/>
        <echo>Building deployment script helper</echo>
        <echo file="${scriptname}">
            #!/usr/bin/env bash
            if [ "$#" -ne 1 ]
            then
            echo "Usage: argument one should be the release number to grab"
            exit 1
            fi
            BASE="releases/${env.JOB_NAME}/${env.JOB_NAME}-$1"
            echo "Attempting to copy $BASE*"
            scp jenkins@build.pc.maricopa.edu:$BASE* .
            echo "Don't forget to run sudo ./robo.phar permissions to update permission"
            echo "If this is a dev deployment, you may need to fix public/.htaccess"
        </echo>
    </target>
    <target name="build-file" description="Creates a .build file">
        <property name="buildfilename" value=".build"/>
        <echo>Building .build file</echo>
        <echo file="${buildfilename}">${env.BUILD_NUMBER}</echo>
    </target>
    <target name="tarballs" description="Build release tarballs" depends="gulp,deploy-script">
        <property name="tarballs.base" value="${env.WORKSPACE}/../../releases/${env.JOB_NAME}/${env.JOB_NAME}-${env.BUILD_NUMBER}"/>
        <echo message="Building release tarballs to ${tarballs.base}."/>
        <tar longfile="gnu" destfile="${tarballs.base}.tar.gz" basedir="${basedir}"
             excludes="resources/**,public/**,config/**,bower_components/**,node_modules/**,.env"
             compression="gzip">
        </tar>
        <tar longfile="gnu" destfile="${tarballs.base}-dev.tar.gz" basedir="${basedir}"
             excludes="bower_components/**,node_modules/**,.env"
             compression="gzip">
        </tar>
    </target>
    <target name="zips" description="Build release zips" depends="gulp,deploy-script">
        <property name="zips.base" value="${env.WORKSPACE}/../../releases/${env.JOB_NAME}/${env.JOB_NAME}-${env.BUILD_NUMBER}"/>
        <echo message="Building release zips to ${zips.base}."/>
        <zip destfile="${zips.base}.zip" basedir="${basedir}"
             excludes="resources/**,public/**,config/**,bower_components/**,node_modules/**,.env">
        </zip>
        <zip destfile="${zips.base}-dev.zip" basedir="${basedir}"
             excludes="bower_components/**,node_modules/**,.env">
        </zip>
    </target>
</project>
