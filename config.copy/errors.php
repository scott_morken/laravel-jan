<?php
return [
    'to'         => explode('|', env('ERROR_EMAIL', 'my.email@college.edu')),
    'master'     => 'layouts.master',
    'email_view' => 'smorken/errors::errors.email',
    'mailer'     => Smorken\Errors\Emailer::class,
    'routes' => [
        'enabled' => false,
        'prefix'  => 'error',
    ],
];
