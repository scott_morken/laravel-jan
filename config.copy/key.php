<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 9:38 AM
 */
return [
    'key_length' => env('KEY_LENGTH', 4),
    'key_salt'   => env('KEY_SALT', 'override this in the .env or you will be sad!'),
];
