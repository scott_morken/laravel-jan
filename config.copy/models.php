<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:21 AM
 */
return [
    'expire'       => [
        ['id' => '1h', 'descr' => '1 Hour', 'time_string' => '+1 hour', 'weight' => 0],
        ['id' => '4h', 'descr' => '4 Hours', 'time_string' => '+4 hours', 'weight' => 5],
        ['id' => '8h', 'descr' => '8 Hours', 'time_string' => '+8 hours', 'weight' => 10],
        ['id' => '12h', 'descr' => '12 Hours', 'time_string' => '+12 hours', 'weight' => 15],
        ['id' => '1d', 'descr' => '1 Day', 'time_string' => '+1 day', 'weight' => 20],
        ['id' => '2d', 'descr' => '2 Days', 'time_string' => '+2 days', 'weight' => 25],
        ['id' => '5d', 'descr' => '5 Days', 'time_string' => '+5 days', 'weight' => 30],
        ['id' => '1w', 'descr' => '1 Week', 'time_string' => '+1 week', 'weight' => 35],
        ['id' => '2w', 'descr' => '2 Weeks', 'time_string' => '+2 weeks', 'weight' => 40],
    ],
    'notification' => [
        ['id' => 'E', 'descr' => 'Email', 'handler' => \App\Contracts\Notify\Email::class, 'vector_id' => 'H'],
        ['id' => 'S', 'descr' => 'SMS', 'handler' => \App\Contracts\Notify\Sms::class, 'vector_id' => 'S'],
        [
            'id'        => 'P',
            'descr'     => 'In Person/Offline',
            'handler'   => \App\Contracts\Notify\Offline::class,
            'vector_id' => 'O',
        ],
    ],
    'vector'       => [
        ['id' => 'H', 'descr' => 'HTTP/S'],
        ['id' => 'O', 'descr' => 'Offline'],
        ['id' => 'S', 'descr' => 'SMS'],
    ],
];
