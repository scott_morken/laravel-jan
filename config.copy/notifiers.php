<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 11:25 AM
 */
return [
    'email'   => [
        'settings' => [
            'view'    => 'notifier.email',
            'subject' => 'Just A Note',
        ],
    ],
    'offline' => [],
    'sms'     => [
        'settings' => [
            'number' => env('TWILIO_NUMBER'),
        ],
    ],
];
