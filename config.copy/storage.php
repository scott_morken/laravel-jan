<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 1:39 PM
 */

return [
    /*
	|--------------------------------------------------------------------------
	| Storage Repository Providers
	|--------------------------------------------------------------------------
	|
	| An array of the interface names with each interface's model (optional) and
    | implementation
    | Used by App\Providers\StorageServiceProvider to bind into the DIC
	|
    | Interfaces live in App\Contracts\Storage
    | Implementations live in App\Storage\[Type]
	*/
    \App\Contracts\Storage\Expire::class       => [
        'impl'  => \App\Storage\Config\Expire::class,
        'model' => \App\Models\Config\Expire::class,
        'vo'    => \App\Models\VO::class,
    ],
    \App\Contracts\Storage\File::class         => [
        'impl'  => \App\Storage\Eloquent\File::class,
        'model' => \App\Models\Eloquent\File::class,
    ],
    \App\Contracts\Storage\Note::class         => [
        'impl'  => \App\Storage\Eloquent\Note::class,
        'model' => \App\Models\Eloquent\Note::class,
    ],
    \App\Contracts\Storage\Notification::class => [
        'impl'  => \App\Storage\Config\Notification::class,
        'model' => \App\Models\Config\Notification::class,
        'vo'    => \App\Models\VO::class,
    ],
    \App\Contracts\Storage\Vector::class       => [
        'impl'  => \App\Storage\Config\Vector::class,
        'model' => \App\Models\Config\Vector::class,
        'vo'    => \App\Models\VO::class,
    ],
];
