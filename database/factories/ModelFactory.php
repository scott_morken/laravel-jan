<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(
    \App\Models\Eloquent\Note::class,
    function (Faker\Generator $faker) {
        return [
            'key'           => str_random(64),
            'data'          => 'some encrypted data',
            'expire_at'     => date('Y-m-d H:i:s', strtotime('+1 hour')),
            'views_allowed' => 1,
            'views'         => 0,
        ];
    }
);

$factory->define(
    \App\Models\Eloquent\File::class,
    function (Faker\Generator $faker) {
        return [
            'note_id'  => $faker->randomNumber(3),
            'filename' => $faker->name . '.' . $faker->fileExtension,
            'size'     => $faker->randomNumber(6),
            'mime'     => $faker->mimeType,
            'data'     => 'some encrypted data',
        ];
    }
);
