<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreateTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'notes',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('key', 64);
                $t->text('data');
                $t->tinyInteger('views')->unsigned()->default(0);
                $t->tinyInteger('views_allowed')->unsigned()->default(0);
                $t->dateTime('expire_at');

                $t->timestamps();

                $t->index('expire_at', 'nt_expire_at_ndx');
                $t->unique('key', 'nt_key_ndx');
            }
        );

        Schema::create(
            'files',
            function (Blueprint $t) {
                $t->increments('id');
                $t->integer('note_id')->unsigned();
                $t->string('filename', 64);
                $t->string('mime', 32);
                $t->integer('size')->unsigned();

                $t->timestamps();

                $t->index('note_id', 'files_note_id_ndx');
            }
        );

        DB::statement("ALTER TABLE `files` ADD `data` LONGBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notes');
        Schema::drop('files');
    }
}
