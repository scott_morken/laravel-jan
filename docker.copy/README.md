#### Docker LAMP stack

* copy `docker.copy` to `docker.name`

```
$ cd docker.name
$ docker-compose up # -d to daemonize it
$ docker exec -it dockername_www_1 bash # start bash shell in www server
```

### Intellij IDEA

* Settings/Languages & Frameworks/PHP/Servers - add a server representing the docker container with path mappings

* add a new Docker Deployment run configuration using the docker/docker-compose.yml file

* run the docker deployment

* debugging should be available with just the listener (no need for a server specific debugger)
