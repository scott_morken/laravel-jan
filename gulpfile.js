var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var minifyCSS = require('gulp-clean-css');
var gutil = require('gulp-util');
var rename = require('gulp-rename');

gulp.task('default', ['js-app', 'copy', 'sass-app']);

gulp.task('js-app', function () {
    gulp.src([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/tether/dist/js/tether.js',
        './node_modules/bootstrap/js/modal.js',
        './node_modules/bootstrap/js/transitions.js',
        './node_modules/bootstrap/js/collapse.js',
        './node_modules/smorken.js/dist/smorken.js',
        './node_modules/app.js/dist/app.core.js',
        './resources/assets/js/app.js'
    ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./public/js'))
        .pipe(uglify())
        .pipe(rename('app.min.js'))
        .pipe(gulp.dest('./public/js'))
        .on('error', gutil.log);
});

gulp.task('copy', function () {
    gulp.src('./node_modules/smorken.js/dist/smorken.js.map')
        .pipe(gulp.dest('./public/js'));
    gulp.src('./node_modules/app.js/dist/app.core.js.map')
        .pipe(gulp.dest('./public/js'));
    gulp.src('./resources/assets/js/limited/**/*.js')
        .pipe(gulp.dest('./public/js/limited'));
    gulp.src('./resources/assets/images/*')
        .pipe(gulp.dest('./public/images'));
});

gulp.task('sass-app', function () {
    gulp.src('./resources/assets/scss/app.scss')
        .pipe(sass())
        .pipe(gulp.dest('./public/css'))
        .pipe(minifyCSS())
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest('./public/css'))
        .on('error', gutil.log);
});
