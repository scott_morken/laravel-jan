## Just A Note

#### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

#### Please read the docs/UPGRADE.md file if you are upgrading

#### Requirements
* PHP 7.0+
* [Composer](https://getcomposer.org/)
* [Node.js](http://nodejs.org/) and [Node Package Manager](https://www.npmjs.org/)

* Install gulp-cli, it is easier to install it globally since it is just the cli for the gulp library

```
$ sudo npm -g install gulp-cli
```

## Installation

* Clone the repo

```
$ git clone git@bitbucket.org:scott_morken/laravel-jan.git
```

* Create your .env files and update them 
 
```
$ cp .env.example .env
```

* Copy *.copy files and directories to their base (can skip if you ran the install script)
    * ie config.copy becomes config
        * config
        * resources
        * public and public/index.php
        * public/.htaccess, public/favicon.ico, public/robots.txt - if needed
        
* Run composer to install vendor files

```
$ composer install
```
       
* Install node package manager (npm)
    * install dependencies:

```
$ npm install
```

* build assets
 
```
$ gulp
```

* Edit files for your environment
    * .env contains most of the common config settings
        * if you are setting up a production server make sure to set
        `APP_ENV=production` and `APP_DEBUG=false`
    * config: check app.php, mail.php and session.php for a good start
    * config/storage.php contains the repository/model/vo bindings
    * resources/views: change views to represent your branding
    * resources/assets contains the .scss files for creating the style sheets and javascript files
        * changes to .scss or .js files will require rerunning 'gulp' to update the public/...

* Create the database and user (make sure to update the .env file)

* Generate your app key

```
$ php artisan key:generate
```

* Run the database migrations.

```
$ php artisan migrate
```

* Set permissions (edit the permissions.sh file for your environment)

```
$ cp permissions.sh.copy permissions.sh
$ chmod +x permissions.sh
$ sudo ./permissions.sh
```

* Set up cron for the scheduler (cleans up old/viewed notes) - cron user is set in .env (CRON_USER key)

```
$ cp docs/example.cron /etc/cron.d/jan_scheduler
$ nano /etc/cron.d/jan_scheduler #edit for your environment
```
    
## Additional Info

* you can create multiple .env.\[environment\] files and use `php artisan env:switch` to
switch between them (it just copies the selected one to the .env file)

* make sure your .env has a generated encryption key, if not run

```
$ php artisan key:generate
```

* you may want to generate production assets/caches

```
$ gulp --production && php artisan config:cache && php artisan route:cache
```

#### Apache

* Copy apache.conf.copy to apache.conf

```
$ cp apache.conf.copy apache.conf
```

* Edit the apache.conf file for your environment and symlink it to apache

~~~~
Ubuntu example
$ sudo ln -s /path/to/my/app/apache.conf /etc/apache2/conf-available/self_service.conf 
$ sudo a2enconf self_service
$ sudo apachectl configtest
$ sudo service apache2 reload
~~~~

#### Where things live

See [laravel.com](http://laravel.com) for details on how the parts work.

__views__ (these represent the view layer of your application and are responsible for
providing the user with what they see): `resources/views/`.  Look in `resources/views/vendor/smorken/views`
for the master template files.  This is where you can override the default layout, provide your
own branding (override the images, css, js, etc), and so on.  The templates are nested.  The outer
template is `layouts/master.blade.php`.  The files in `includes` are the header, footer and navigation
templates.

__assets__ (less, js, images): `resources/assets/`

__config__ (sets up how everything should work, uses .env to populate private/changing info):
`config/`

__business logic__ (controllers, models, classes, everything that makes up the domain of your application):
`app/` which correlates to the namespace __App__. There is also a `local/` directory which has a namespace
of __Local__. It is git ignored so you can create any overrides, additional stuff there without
worrying about git eating it.

__storage__: you can override storage providers (repositories) in the `config/storage.php file` or by
adding a new service provider (like `App\Providers\StorageServiceProviders`) and replacing it in the
`config/app.php` file.  The repository classes live in `App\Storage`.

#### Errors? What errors?

Edit the .env file and set the `ERROR_EMAIL` key to your email (or wherever you want errors sent).

Edit the `config/mail.php` file and set up your environment (smtp settings, from settings)

Edit the `config/errors.php` file, ensure everything is to your liking.  On a production server you will
likely want to set `routes.enabled` to false.

There are four test error routes for your testing fun.  If `APP_DEBUG` is false, you will even
get the email results if everything is set up correctly.

* error/exception - triggers an exception
* error/error - triggers an error (which triggers \ErrorException)
* error/404 - triggers a 404
* error/403 - triggers a 403
