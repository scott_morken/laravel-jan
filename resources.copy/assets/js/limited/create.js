/**
 * Created by smorken on 2/27/15.
 */
(function (wHnd, $) {
    var f =
        {
            init: function () {
                this.addListeners();
            },
            addListeners: function () {
                $.each(this.listeners, function (i, func) {
                    func();
                });
            },
            listeners: [
                function () {
                    $('form').on('click', '#new-key', function (event) {
                        f.ajax.getKey();
                    });
                },
                function () {
                    $('form').on('change', '.comp', function (event) {
                        f.render.clearErrors();
                        f.validate.vectors();
                    });
                },
                function () {
                    $('form').submit(function (event) {
                        $(this).find('input[type=submit]').prop('disabled', true);
                        f.render.submitMessage('Please wait...');
                    });
                }
            ],
            ajax: {
                getKey: function () {
                    var url = app.frontend.getUrl('new-key');
                    var success = function (data) {
                        $('#key-value').val(data.key);
                        $('#key-display').text(data.key);
                    };
                    var fail = function (data) {
                        app.frontend.ajaxFail(data);
                    };
                    $.ajax({
                        url: url,
                        success: success,
                        error: fail,
                        dataType: 'json'
                    });
                }
            },
            validate: {
                vectors: function () {
                    var v1 = $('#key-notification-id option:selected').data('vector');
                    var v2 = $('#shared-notification-id option:selected').data('vector');
                    if (v1 && v1 === v2) {
                        f.render.vectors();
                        return false;
                    }
                    return true;
                }
            },
            render: {
                submitMessage: function (msg) {
                    $('#submit-msg').text(msg);
                },
                vectors: function () {
                    f.render.addError(f.render.error('Please select two notifiers that use different vectors. ' +
                        '(eg, Email and Offline). You may continue with your current selection, but it may not be secure.'));
                },
                error: function (msg) {
                    var ele = $('<div/>');
                    ele.addClass('alert alert-danger');
                    ele.text(msg);
                    return ele;
                },
                addError: function (error) {
                    $('#val-errors').show();
                    $('#val-errors').append(error);
                },
                clearErrors: function () {
                    $('#val-errors').html('');
                    $('#val-errors').hide();
                }
            }
        };
    smorken.Extend('app.create', f);
})(window, jQuery);

smorken.ready(function () {
    app.create.init();
});
