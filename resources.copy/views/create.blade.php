@extends($master)
@section('content')
    {!! Form::open(['action' => $controller . '@saveNote', 'files' => true]) !!}
    <div class="row">
        <div class="form-group col-sm-6">
            {!! Form::label('key-value', 'Key', ['class' => 'control-label']) !!}
            <button type="button" id="new-key" class="btn btn-sm btn-outline-success float-sm-right">Regen</button>
            <input type="hidden" name="key[value]" id="key-value" value="{{ $key }}"/>
            <div class="form-control-static" id="key-display">{{ $key }}</div>
        </div>
        <div class="form-group col-sm-3">
            {!! Form::label('key-notification-id', 'Notifier', ['class' => 'control-label']) !!}
            @include('partials._select_list', [
                'name' => 'key[notification_id]',
                'id' => 'key-notification-id',
                'default' => old('key.notification_id') ?: 'E',
                'list_items' => $notifiers,
                'classes' => 'comp',
            ])
        </div>
        <div class="form-group col-sm-3">
            {!! Form::label('key-to', 'To', ['class' => 'control-label']) !!}
            {!! Form::text('key[to]', null, ['class' => 'form-control', 'id' => 'key-to', 'maxlength' => 128]) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            {!! Form::label('shared-value', 'Shared Knowledge', ['class' => 'control-label']) !!}
            {!! Form::text(
            'shared[value]',
            null,
            ['class' => 'form-control', 'id' => 'shared-value', 'autocomplete' => 'off', 'maxlength' => 128]
            ) !!}
        </div>
        <div class="form-group col-sm-3">
            {!! Form::label('shared-notification-id', 'Notifier', ['class' => 'control-label']) !!}
            @include('partials._select_list', [
                'name' => 'shared[notification_id]',
                'id' => 'shared-notification-id',
                'default' => old('shared.notification_id') ?: 'P',
                'list_items' => $notifiers,
                'classes' => 'comp',
            ])
        </div>
        <div class="form-group col-sm-3">
            {!! Form::label('shared-to', 'To', ['class' => 'control-label']) !!}
            {!! Form::text(
            'shared[to]',
            null,
            ['class' => 'form-control', 'id' => 'shared-to', 'maxlength' => 128]
            ) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            {!! Form::label('views_allowed', 'Views Allowed', ['class' => 'control-label']) !!}
            {!! Form::number('views_allowed', 1, ['class' => 'form-control', 'maxlength' => 128]) !!}
        </div>
        <div class="form-group col-sm-6">
            {!! Form::label('expire_at', 'Expires', ['class' => 'control-label']) !!}
            {!! Form::select(
                'expire_at',
                select_list($expires, 'id', 'descr'),
                '8h',
                 ['class' => 'form-control', 'id' => 'shared-notification-id']
            ) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('note', 'Note') !!}
        {!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => 5, 'autocomplete' => 'off']) !!}
    </div>
    <div class="row" id="files">
        <div class="col-sm-6">
            <div class="form-group">
                <input type="file" name="files[]" class="file-select form-control-file" multiple/>
            </div>
        </div>
        <div class="col-sm-6">
            You can select and attach multiple files.
            <span class="text-warning">Max: {{ \Illuminate\Http\UploadedFile::getMaxFilesize()/(1024*1024) }} MB</span>
        </div>
    </div>
    <div class="form-group">
        {!! Captcha::display() !!}
        {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        <span class="text-primary" id="submit-msg"></span>
    </div>
    {!! Form::close() !!}
    <div id="val-errors"></div>
@stop
@section('foot-js')
    {!! HTML::script('js/limited/create.js') !!}
    <script>
        app.frontend.urls = app.frontend.urls || {};
        app.frontend.urls['new-key'] = '{{ action('HomeController@genKey') }}';
    </script>
    {!! Captcha::script() !!}
@stop
