@extends($master)
@section('content')

    {!! Form::open(['action' => $controller . '@retrieve', 'autocomplete' => 'off']) !!}
    <div class="form-group">
        {!! Form::label('key', 'Key', ['class' => 'control-label']) !!}
        {!! Form::number('key', null, ['class' => 'form-control', 'autocomplete' => 'off', 'maxlength' => 128]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('shared', 'Shared Knowledge', ['class' => 'control-label']) !!}
        {!! Form::text('shared', null, ['class' => 'form-control', 'id' => 'shared', 'autocomplete' => 'off', 'maxlength' => 128]) !!}
    </div>
    <div>
        {!! Captcha::display() !!}
        {!! Form::submit('Retrieve', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
    {!! Captcha::script() !!}
@stop
