<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <?php $encrypter = app('Illuminate\Encryption\Encrypter'); ?>
    <meta name="csrf-token" content="<?php echo $encrypter->encrypt(csrf_token()); ?>">
    <title>
        {{ config('app.title', 'Just a Note') }}
    </title>
    <!-- style -->
    <link rel="stylesheet" type="text/css" href="{!! asset('css/app.css') !!}">
</head>
<body>
<div class="container">
    <div class="loading"></div>
    <div class="masthead clearfix">
        <div class="inner">
            <h3 class="masthead-brand">{{ config('app.title', 'Just A Note') }}</h3>
            <nav class="nav nav-masthead">
                <a class="nav-link" href="{{ action('HomeController@index') }}">Home</a>
                <a class="nav-link" href="{{ action('HomeController@create') }}">New</a>
            </nav>
        </div>
    </div>

    <div class="inner cover">
        <!--[if lt IE 8]>
        <p class="alert alert-warning">You are using an <strong>outdated</strong> browser. Please <a
                href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        @if (class_exists('HTML'))
            {!! HTML::messages() !!}
        @endif
        <div id="content">
            @yield('content')
        </div>
    </div>
    <div class="mastfoot">
        <div class="inner">
            <p>To ensure the security of your note, please close this window when you have finished.</p>
            <p>{!! link_to('https://bitbucket.org/scott_morken/laravel-jan', 'Just A Note') !!}</p>
        </div>
    </div>

</div>
<div class="container">

</div>
<footer class="footer">
</footer>
@include('partials._modal')
@if (app()->environment() === 'production')
    {!! HTML::script('js/app.min.js') !!}
@else
    {!! HTML::script('js/app.js') !!}
@endif
@yield('foot-js')
</body>
</html>
