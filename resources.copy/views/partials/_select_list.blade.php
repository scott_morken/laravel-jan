<select name="{{ $name }}" class="form-control {{ $classes }}" id="{{ $id }}">
    @foreach($list_items as $item)
        <option value="{{ $item->id }}" {{ $item->id == $default ? 'selected' : null }}
        data-vector="{{ $item->vector_id }}">{{ $item->descr }}</option>
    @endforeach
</select>
