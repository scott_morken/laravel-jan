<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/19/14
 * Time: 3:07 PM
 */
/* @var string $message */
/* @var string $type */
?>
<div class="alert alert-{{ $type }} alert-dismissable">
    {!! $message !!}
</div>
