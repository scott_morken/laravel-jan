@extends($master)
@section('content')
    @if ($note)
        <div class="card" id="note">
            <div class="card-header">
                <h3 class="card-title">Your note</h3>
                <h6 class="card-subtitle">Views: {{ $note->views }} / {{ $note->views_allowed }}</h6>
            </div>
            <div class="card-block">
                <div id="note-view">
                    {!! nl2br(e($note->data)) !!}
                </div>
                @if ($note->files && count($note->files))
                    <hr/>
                    <div id="file-list">
                        <h6>Files</h6>
                        <ul>
                            @foreach($note->files as $file)
                                <li>
                                    <a href="{{ action($controller . '@file', ['id' => $file->id, 'token' => $token]) }}">
                                        {{ $file->filename }}
                                    </a>
                                    ({{ $file->size }} bytes)
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="card-footer text-center">
                Please close this window when you have read your note.
            </div>
        </div>
    @else
        <div class="card">
            <div class="card-block">
                The requested note could not be found. It may have expired, reached its max views or does not exist.
            </div>
        </div>
    @endif
@stop
@section('foot-js')
    <script>
        history.replaceState({}, 'Home', '{{ action('HomeController@index') }}');
    </script>
@stop
