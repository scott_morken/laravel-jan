<?php
Route::get('/', 'HomeController@index');
Route::post('retrieve', 'HomeController@retrieve');
Route::get('file/{id}/{token}', 'HomeController@file');

Route::get('create', 'HomeController@create');
Route::post('save-note', 'HomeController@saveNote');

Route::get('gen-key', 'HomeController@genKey');
