#!/usr/bin/env bash
tar --exclude=".git" --exclude="./.idea" --exclude="./bower_components" --exclude="./config.copy" --exclude="./docker*" --exclude="./node_modules" --exclude="./public.copy" --exclude="./resources.copy" --exclude="./.git" --exclude="./.env" -cvzf project.tar.gz .
