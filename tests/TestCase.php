<?php
namespace Tests\App;

use App\Exceptions\Handler;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Monolog\Logger;

class TestCase extends \Laravel\BrowserKitTesting\TestCase {

    use DatabaseTransactions, DatabaseMigrations;
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    protected $oldExceptionHandler;

    public static $initLog = false;

	/**
	 * Creates the application.
	 *
	 * @return \Illuminate\Foundation\Application
	 */
	public function createApplication()
	{
		$app = require __DIR__.'/../bootstrap/app.php';

		$app->make(Kernel::class)->bootstrap();
		if (!static::$initLog) {
		    file_put_contents(__DIR__ . '/tests.log', '');
		    static::$initLog = true;
        }
        $handler = new \Monolog\Handler\StreamHandler(__DIR__ . '/tests.log', Logger::ERROR);
        $handler->setFormatter(new \Monolog\Formatter\LineFormatter(null, null, true));
        \Log::getMonolog()->setHandlers([$handler]);
		return $app;
	}

    /**
     * Define hooks to migrate the database before and after each test.
     *
     * @return void
     */
    public function runDatabaseMigrations()
    {
        $this->artisan('migrate');

        $this->app[Kernel::class]->setArtisan(null);

        $this->beforeApplicationDestroyed(
            function () {
                $this->artisan('migrate:rollback');
            }
        );
        $this->seed();
    }

    protected function storeArrayInput($values, $name)
    {
        $this->inputs[$name] = $values;
        return $this;
    }

    protected function disableExceptionHandling()
    {
        $this->oldExceptionHandler = $this->app->make(ExceptionHandler::class);
        $this->app->instance(ExceptionHandler::class, new class extends Handler {
            public function __construct() {}
            public function report(\Exception $e) {}
            public function render($request, \Exception $e) {
                throw $e;
            }
        });
    }

    protected function withExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, $this->oldExceptionHandler);
        return $this;
    }
}
