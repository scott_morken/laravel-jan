<?php

namespace Tests\App\end2end\Controllers;

use App\Contracts\Key\Hash;
use App\Models\Eloquent\File;
use App\Models\Eloquent\Note;
use Illuminate\Contracts\Encryption\Encrypter;
use Tests\App\TestCase;

class HomeE2ETest extends TestCase
{

    public function testHomeRoute()
    {
        $response = $this->call('GET', '/');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testClickRetrieveShowsValidationErrors()
    {
        $this->visit('/')
             ->press('Retrieve')
             ->see('The key field is required')
             ->see('The shared field is required');
    }

    public function testRetrieveWithNonExistantIsError()
    {
        $this->visit('/')
             ->type('123', 'key')
             ->type('foobar', 'shared')
             ->press('Retrieve')
             ->seePageIs('/')
             ->see('The requested note could not be found');
    }

    public function testRetrieveWithExpiredIsError()
    {
        $h = $this->app->make(Hash::class);
        $m = factory(Note::class)->create(
            [
                'key'       => $h->create('123', 'foobar'),
                'data'      => '',
                'expire_at' => date('Y-m-d H:i:s', strtotime('-1 hour')),
            ]
        );
        $this->visit('/')
             ->type('123', 'key')
             ->type('foobar', 'shared')
             ->press('Retrieve')
             ->seePageIs('/')
             ->see('The requested note could not be found');
    }

    public function testRetrieveWithViewedIsError()
    {
        $h = $this->app->make(Hash::class);
        $m = factory(Note::class)->create(
            [
                'key'   => $h->create('123', 'foobar'),
                'data'  => '',
                'views' => 1,
            ]
        );
        $this->visit('/')
             ->type('123', 'key')
             ->type('foobar', 'shared')
             ->press('Retrieve')
             ->seePageIs('/')
             ->see('The requested note could not be found');
    }

    public function testRetrieveSuccess()
    {
        config()->set('app.key', 'SomeRandomString32CharsLongNotIt');
        $h = $this->app->make(Hash::class);
        $e = $this->app->make(Encrypter::class);
        $m = factory(Note::class)->create(
            [
                'key'   => $h->create('123', 'foobar'),
                'data'  => $e->encrypt('test'),
                'views' => 0,
            ]
        );
        $this->visit('/')
             ->type('123', 'key')
             ->type('foobar', 'shared')
             ->press('Retrieve')
             ->see('test');
    }

    public function testRetrieveSuccessWithFiles()
    {
        config()->set('app.key', 'SomeRandomString32CharsLongNotIt');
        $h = $this->app->make(Hash::class);
        $e = $this->app->make(Encrypter::class);
        $m = factory(Note::class)->create(
            [
                'key'   => $h->create('123', 'foobar'),
                'data'  => $e->encrypt('test'),
                'views' => 0,
            ]
        );
        $f1 = factory(File::class)->create(
            [
                'note_id'  => $m->id,
                'data'     => $e->encrypt('test text file'),
                'filename' => 'test1.txt',
                'size'     => '14',
                'mime'     => 'text/plain',
            ]
        );
        $f2 = factory(File::class)->create(
            [
                'note_id'  => $m->id,
                'data'     => $e->encrypt('test text file 2'),
                'filename' => 'test2.txt',
                'size'     => '16',
                'mime'     => 'text/plain',
            ]
        );
        $this->visit('/')
             ->type('123', 'key')
             ->type('foobar', 'shared')
             ->press('Retrieve')
             ->see('test')
             ->see($f1->filename)
             ->see($f2->filename);
    }

    public function testCreateNoteValidationErrors()
    {
        $this->visit('/create')
             ->press('Create')
             ->see('The key.to field is required')
             ->see('The shared.value field is required')
             ->see('The note field is required');
    }

    public function testCreateSuccess()
    {
        $this->visit('/create')
             ->storeArrayInput(['value' => 1, 'to' => 'email@example.org'], 'key')
             ->storeArrayInput(['value' => 'super secret'], 'shared')
             ->type('you need your decoder ring to read this', 'note')
             ->press('Create')
             ->seePageIs('/')
             ->see('Your note has been created')
             ->type('1', 'key')
             ->type('super secret', 'shared')
             ->press('Retrieve')
             ->see('you need your decoder ring to read this');
    }
}
