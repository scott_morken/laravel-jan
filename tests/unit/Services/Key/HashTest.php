<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 10:55 AM
 */

namespace Tests\App\unit\Services\Key;

use App\Contracts\Storage\Note;
use App\Services\Key\Hash;
use App\Services\Key\KeyException;
use Mockery as m;

class HashTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testShortSaltIsException()
    {
        $this->expectException(KeyException::class);
        $this->expectExceptionMessage('Salt must be at least 8 characters long');
        $this->getSut('foo');
    }

    public function testCollisionIsException()
    {
        $this->expectException(KeyException::class);
        $this->expectExceptionMessage('Please enter a different shared secret or key');
        list($sut, $np) = $this->getSut();
        $np->shouldReceive('getByKey')->once()->with(
            '453781ae092dd93fb0f0605007c0e65c5553a459783a31a6fcbb755c6e0c8b47',
            false
        )->andReturn(
            m::mock(\App\Contracts\Models\Note::class)
        );
        $sut->create(1234, 'super cool secret');
    }

    public function testCanReturnHash()
    {
        list($sut, $np) = $this->getSut();
        $np->shouldReceive('getByKey')->once()->with(
            '453781ae092dd93fb0f0605007c0e65c5553a459783a31a6fcbb755c6e0c8b47',
            false
        )->andReturnNull();
        $hash = $sut->create(1234, 'super cool secret');
        $this->assertEquals('453781ae092dd93fb0f0605007c0e65c5553a459783a31a6fcbb755c6e0c8b47', $hash);
    }

    protected function getSut($salt = 'foobar_fizbuz')
    {
        $np = m::mock(Note::class);
        $sut = new Hash($np, $salt);
        return [$sut, $np];
    }
}
