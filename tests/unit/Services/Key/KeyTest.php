<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/5/17
 * Time: 10:48 AM
 */

namespace Tests\App\unit\Services\Key;

use App\Services\Key\Key;

class KeyTest extends \PHPUnit\Framework\TestCase
{

    public function testKeyMinIsTwo()
    {
        $sut = $this->getSut(1);
        $this->assertEquals(2, $sut->getLength());
    }

    public function testKeyMaxIsSixteen()
    {
        $sut = $this->getSut(1000);
        $this->assertEquals(16, $sut->getLength());
    }

    public function testGetIsInRange()
    {
        $sut = $this->getSut(2);
        for ($i = 0; $i < 100; $i ++) {
            $r = $sut->get();
            $this->assertLessThanOrEqual(99, $r);
            $this->assertGreaterThanOrEqual(1, $r);
        }
    }

    public function getSut($length = 4)
    {
        return new Key($length);
    }
}
