<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 6:04 PM
 */

namespace Tests\App\unit\Services\Note;

use App\Contracts\Note\KeySharedBuilder;
use App\Contracts\Storage\Expire;
use App\Contracts\Storage\File;
use App\Contracts\Storage\Note;
use App\Services\Key\KeyException;
use App\Services\Note\Builder;
use App\Services\Note\BuilderException;
use App\Services\Notifiers\NotifyException;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\UploadedFile;
use Mockery as m;

class BuilderTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testBuildWithKeyException()
    {
        list($sut, $ksb, $note, $expire, $enc, $fp, $log) = $this->getSut();
        $data = [
            'key'           => [

            ],
            'shared'        => [

            ],
            'views_allowed' => 1,
            'note'          => 'foo text',
            'expire_at'     => '1h',
        ];
        $ke = new KeyException('key exception');
        $ksb->shouldReceive('build')->with($data)->andThrow($ke);
        $log->shouldReceive('error')->with($ke);
        $this->assertFalse($sut->build($data));
        $this->assertCount(1, $sut->getErrors());
    }

    public function testBuildWithBuilderException()
    {
        list($sut, $ksb, $note, $expire, $enc, $fp, $log) = $this->getSut();
        $data = [
            'key'           => [

            ],
            'shared'        => [

            ],
            'views_allowed' => 1,
            'note'          => 'foo text',
            'expire_at'     => '1h',
        ];
        $be = new BuilderException('builder exception');
        $ksb->shouldReceive('build')->with($data)->andThrow($be);
        $log->shouldReceive('error')->with($be);
        $this->assertFalse($sut->build($data));
        $this->assertCount(1, $sut->getErrors());
    }

    public function testBuildWithCreationFailure()
    {
        list($sut, $ksb, $note, $expire, $enc, $fp, $log) = $this->getSut();
        $data = [
            'key'           => [

            ],
            'shared'        => [

            ],
            'views_allowed' => 1,
            'note'          => 'foo text',
            'expire_at'     => '1h',
        ];
        $e = m::mock(\App\Contracts\Models\Expire::class);
        $e->time_string = '+2 days';
        $ksb->shouldReceive('build')->with($data);
        $ksb->shouldReceive('getHash')->andReturn('hash value');
        $enc->shouldReceive('encrypt')->with('foo text')->andReturn('encrypted');
        $expire->shouldReceive('find')->with('1h')->andReturn($e);
        $note->shouldReceive('create')->with(
            m::on(
                function ($data) {
                    return ($data['key'] === 'hash value' && $data['data'] === 'encrypted' && $data['views_allowed'] === 1 && $data['views'] === 0);
                }
            )
        )->andReturn(false);
        $this->assertFalse($sut->build($data));
    }

    public function testBuildWithNotifyFailureIsException()
    {
        list($sut, $ksb, $note, $expire, $enc, $fp, $log) = $this->getSut();
        $data = [
            'key'           => [

            ],
            'shared'        => [

            ],
            'views_allowed' => 1,
            'note'          => 'foo text',
            'expire_at'     => '1h',
        ];
        $e = m::mock(\App\Contracts\Models\Expire::class);
        $e->time_string = '+2 days';
        $ksb->shouldReceive('build')->with($data);
        $ksb->shouldReceive('getHash')->andReturn('hash value');
        $enc->shouldReceive('encrypt')->with('foo text')->andReturn('encrypted');
        $expire->shouldReceive('find')->with('1h')->andReturn($e);
        $nm = m::mock(\App\Contracts\Models\Note::class);
        $note->shouldReceive('create')->with(
            m::on(
                function ($data) {
                    return ($data['key'] === 'hash value' && $data['data'] === 'encrypted' && $data['views_allowed'] === 1 && $data['views'] === 0);
                }
            )
        )->andReturn($nm);
        $ksb->shouldReceive('notify')->andReturn(['key' => false, 'shared' => true]);
        $this->expectException(NotifyException::class);
        $this->expectExceptionMessage('There was an error sending notifications (key), you will need to do so manually.');
        $sut->build($data);
    }

    public function testBuildSuccessful()
    {
        list($sut, $ksb, $note, $expire, $enc, $fp, $log) = $this->getSut();
        $data = [
            'key'           => [

            ],
            'shared'        => [

            ],
            'views_allowed' => 1,
            'note'          => 'foo text',
            'expire_at'     => '1h',
        ];
        $e = m::mock(\App\Contracts\Models\Expire::class);
        $e->time_string = '+2 days';
        $ksb->shouldReceive('build')->with($data);
        $ksb->shouldReceive('getHash')->andReturn('hash value');
        $enc->shouldReceive('encrypt')->with('foo text')->andReturn('encrypted');
        $expire->shouldReceive('find')->with('1h')->andReturn($e);
        $nm = m::mock(\App\Contracts\Models\Note::class);
        $note->shouldReceive('create')->with(
            m::on(
                function ($data) {
                    return ($data['key'] === 'hash value' && $data['data'] === 'encrypted' && $data['views_allowed'] === 1 && $data['views'] === 0);
                }
            )
        )->andReturn($nm);
        $ksb->shouldReceive('notify')->andReturn(['key' => true, 'shared' => true]);
        $this->assertTrue($sut->build($data));
    }

    public function testBuildSuccessfulWithFiles()
    {
        list($sut, $ksb, $note, $expire, $enc, $fp, $log) = $this->getSut();
        $files = [
            m::mock(UploadedFile::class),
        ];
        $data = [
            'key'           => [

            ],
            'shared'        => [

            ],
            'views_allowed' => 1,
            'note'          => 'foo text',
            'expire_at'     => '1h',
            'files'         => $files,
        ];
        $e = m::mock(\App\Contracts\Models\Expire::class);
        $e->time_string = '+2 days';
        $ksb->shouldReceive('build')->with($data);
        $ksb->shouldReceive('getHash')->andReturn('hash value');
        $enc->shouldReceive('encrypt')->once()->with('foo text')->andReturn('encrypted');
        $expire->shouldReceive('find')->with('1h')->andReturn($e);
        $nm = m::mock(\App\Contracts\Models\Note::class);
        $nm->id = 1;
        $note->shouldReceive('create')->with(
            m::on(
                function ($data) {
                    return ($data['key'] === 'hash value' && $data['data'] === 'encrypted' && $data['views_allowed'] === 1 && $data['views'] === 0);
                }
            )
        )->andReturn($nm);
        $files[0]->shouldReceive('isValid')->once()->andReturn(true);
        $files[0]->shouldReceive('path')->once()->andReturn(__DIR__ . '/dummy.txt');
        $enc->shouldReceive('encrypt')->once()->with("foo bar\n")->andReturn('foo bar encrypted');
        $files[0]->shouldReceive('getMimeType')->once()->andReturn('text/plain');
        $files[0]->shouldReceive('getSize')->once()->andReturn(10);
        $files[0]->shouldReceive('getClientOriginalName')->once()->andReturn('dummy.txt');
        $fp->shouldReceive('create')->once()->with(
            [
                'note_id'  => 1,
                'mime'     => 'text/plain',
                'size'     => 10,
                'filename' => 'dummy.txt',
                'data'     => 'foo bar encrypted',
            ]
        )->andReturn(true);
        $ksb->shouldReceive('notify')->andReturn(['key' => true, 'shared' => true]);
        $this->assertTrue($sut->build($data));
    }

    protected function getSut()
    {
        $ksb = m::mock(KeySharedBuilder::class);
        $np = m::mock(Note::class);
        $ep = m::mock(Expire::class);
        $enc = m::mock(Encrypter::class);
        $fp = m::mock(File::class);
        $l = m::mock(Log::class);
        $sut = new Builder($ksb, $np, $ep, $enc, $fp, $l);
        return [$sut, $ksb, $np, $ep, $enc, $fp, $l];
    }
}
