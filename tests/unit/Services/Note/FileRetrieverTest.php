<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 6:05 PM
 */

namespace Tests\App\unit\Services\Note;

use App\Contracts\Storage\File;
use App\Services\Note\FileRetriever;
use Illuminate\Contracts\Encryption\Encrypter;
use Mockery as m;

class FileRetrieverTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testGetNotFoundIsNull()
    {
        list($sut, $enc, $fp) = $this->getSut();
        $fp->shouldReceive('find')->with(123)->andReturn(null);
        $this->assertNull($sut->get(123));
    }

    public function testGetDecryptsDataAttribute()
    {
        list($sut, $enc, $fp) = $this->getSut();
        $m = m::mock(\App\Contracts\Models\File::class);
        $m->data = 'encrypted';
        $fp->shouldReceive('find')->with(123)->andReturn($m);
        $enc->shouldReceive('decrypt')->with('encrypted')->andReturn('decrypted');
        $m = $sut->get(123);
        $this->assertEquals('decrypted', $m->data);
    }

    protected function getSut()
    {
        $e = m::mock(Encrypter::class);
        $fp = m::mock(File::class);
        $sut = new FileRetriever($e, $fp);
        return [$sut, $e, $fp];
    }
}
