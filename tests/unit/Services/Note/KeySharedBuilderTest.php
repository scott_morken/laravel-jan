<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 5:50 PM
 */

namespace Tests\App\unit\Services\Note;

use App\Contracts\Key\Hash;
use App\Contracts\Notify\Factory;
use App\Contracts\Notify\Notifier;
use App\Contracts\Storage\Notification;
use App\Services\Note\KeySharedBuilder;
use Illuminate\Contracts\Routing\UrlGenerator;
use Mockery as m;

class KeySharedBuilderTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testNoNotificationIdIsException()
    {
        $data = [
            'key'    => [
                'notification_id' => null,
            ],
            'shared' => [
                'notification_id' => null,
            ],
        ];
        $this->expectExceptionMessage('No notification provided.');
        list($sut, $factory, $hash, $provider, $notifier) = $this->getSut();
        $sut->build($data);
    }

    public function testNoNotificationIdFoundIsException()
    {
        $data = [
            'key'    => [
                'notification_id' => 1,
            ],
            'shared' => [
                'notification_id' => 1,
            ],
        ];
        $this->expectExceptionMessage('Unable to find notification.');
        list($sut, $factory, $hash, $provider, $notifier) = $this->getSut();
        $provider->shouldReceive('find')->with(1)->andReturn(null);
        $sut->build($data);
    }

    public function testNotificationIdFoundWithNoNotifierIsException()
    {
        $data = [
            'key'    => [
                'notification_id' => 1,
            ],
            'shared' => [
                'notification_id' => 1,
            ],
        ];
        $this->expectExceptionMessage('Unable to create notifier.');
        list($sut, $factory, $hash, $provider, $notifier) = $this->getSut();
        $n = m::mock(\App\Contracts\Models\Notification::class);
        $n->handler = 'foo_handler';
        $provider->shouldReceive('find')->with(1)->andReturn($n);
        $factory->shouldReceive('make')->with('foo_handler')->andReturn(null);
        $sut->build($data);
    }

    public function testNotificationIdFoundWithNotifierWithoutAllComponentsIsException()
    {
        $data = [
            'key'    => [
                'notification_id' => 1,
                //'value'           => 'value_key',
                'to'              => 'to_key',
            ],
            'shared' => [
                'notification_id' => 1,
                'value'           => 'value_shared',
                'to'              => 'to_shared',
            ],
        ];
        list($sut, $factory, $hash, $provider, $notifier) = $this->getSut();
        $n = m::mock(\App\Contracts\Models\Notification::class);
        $n->handler = 'foo_handler';
        $provider->shouldReceive('find')->once()->with(1)->andReturn($n);
        $factory->shouldReceive('make')->twice()->with('foo_handler')->andReturn($notifier);
        $this->expectExceptionMessage('Missing required component(s).');
        $sut->build($data);
    }

    public function testNotificationIdFoundWithNotifier()
    {
        $data = [
            'key'    => [
                'notification_id' => 1,
                'value'           => 'value_key',
                'to'              => 'to_key',
            ],
            'shared' => [
                'notification_id' => 1,
                'value'           => 'value_shared',
                'to'              => 'to_shared',
            ],
        ];
        list($sut, $factory, $hash, $provider, $notifier) = $this->getSut();
        $n = m::mock(\App\Contracts\Models\Notification::class);
        $n->handler = 'foo_handler';
        $provider->shouldReceive('find')->once()->with(1)->andReturn($n);
        $factory->shouldReceive('make')->twice()->with('foo_handler')->andReturn($notifier);
        $sut->build($data);
        $this->assertEquals('value_key', $sut->getValue('key'));
        $this->assertEquals('to_key', $sut->getTo('key'));
        $this->assertEquals($notifier, $sut->getNotifier('key'));
    }

    public function testGetHash()
    {
        $data = [
            'key'    => [
                'notification_id' => 1,
                'value'           => 'value_key',
                'to'              => 'to_key',
            ],
            'shared' => [
                'notification_id' => 1,
                'value'           => 'value_shared',
                'to'              => 'to_shared',
            ],
        ];
        list($sut, $factory, $hash, $provider, $notifier) = $this->getSut();
        $n = m::mock(\App\Contracts\Models\Notification::class);
        $n->handler = 'foo_handler';
        $provider->shouldReceive('find')->once()->with(1)->andReturn($n);
        $factory->shouldReceive('make')->twice()->with('foo_handler')->andReturn($notifier);
        $hash->shouldReceive('create')->once()->with('value_key', 'value_shared')->andReturn('hashed string');
        $sut->build($data);
        $this->assertEquals('hashed string', $sut->getHash());
    }

    public function testNotify()
    {
        $data = [
            'key'    => [
                'notification_id' => 1,
                'value'           => 'value_key',
                'to'              => 'to_key',
            ],
            'shared' => [
                'notification_id' => 1,
                'value'           => 'value_shared',
                'to'              => 'to_shared',
            ],
        ];
        list($sut, $factory, $hash, $provider, $notifier) = $this->getSut();
        $n = m::mock(\App\Contracts\Models\Notification::class);
        $n->handler = 'foo_handler';
        $provider->shouldReceive('find')->once()->with(1)->andReturn($n);
        $factory->shouldReceive('make')->twice()->with('foo_handler')->andReturn($notifier);
        $notifier->shouldReceive('send')->once()->with(
            'to_key',
            ['msg' => 'Key: value_key', 'url' => 'url']
        )->andReturn(true);
        $notifier->shouldReceive('send')->once()->with(
            'to_shared',
            ['msg' => 'Shared Knowledge: value_shared', 'url' => 'url']
        )->andReturn(true);
        $sut->build($data);
        $this->assertEquals(['key' => true, 'shared' => true], $sut->notify());
    }

    protected function getSut()
    {
        $f = m::mock(Factory::class);
        $n = m::mock(Notifier::class);
        $h = m::mock(Hash::class);
        $np = m::mock(Notification::class);
        $url = m::mock(UrlGenerator::class);
        $url->shouldReceive('to')->andReturn('url');
        $sut = new KeySharedBuilder($f, $h, $np, $url);
        return [$sut, $f, $h, $np, $n];
    }
}
