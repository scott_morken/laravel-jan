<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/12/17
 * Time: 6:05 PM
 */

namespace Tests\App\unit\Services\Note;

use App\Contracts\Key\Hash;
use App\Contracts\Storage\Note;
use App\Services\Note\Retriever;
use Illuminate\Contracts\Encryption\Encrypter;
use Mockery as m;

class RetrieverTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testGetNotFoundIsNull()
    {
        list($sut, $enc, $note, $hash) = $this->getSut();
        $hash->shouldReceive('create')->with(123, 'bar', false)->andReturn('hashed');
        $note->shouldReceive('getByKey')->with('hashed')->andReturn(null);
        $this->assertNull($sut->get(123, 'bar'));
    }

    public function testGetDecryptsDataAttribute()
    {
        list($sut, $enc, $note, $hash) = $this->getSut();
        $m = m::mock(\App\Contracts\Models\Note::class);
        $m->data = 'encrypted';
        $hash->shouldReceive('create')->with(123, 'bar', false)->andReturn('hashed');
        $note->shouldReceive('getByKey')->with('hashed')->andReturn($m);
        $enc->shouldReceive('decrypt')->with('encrypted')->andReturn('decrypted');
        $m = $sut->get(123, 'bar');
        $this->assertEquals('decrypted', $m->data);
    }

    protected function getSut()
    {
        $e = m::mock(Encrypter::class);
        $np = m::mock(Note::class);
        $h = m::mock(Hash::class);
        $sut = new Retriever($e, $np, $h);
        return [$sut, $e, $np, $h];
    }
}
