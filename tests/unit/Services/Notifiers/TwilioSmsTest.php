<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/13/17
 * Time: 4:30 PM
 */

namespace Tests\App\unit\Services\Notifiers {

    use App\Services\Notifiers\TwilioSms;
    use Illuminate\Contracts\Logging\Log;
    use Mockery as m;
    use Twilio\Exceptions\TwilioException;
    use Twilio\Http\Client;

    class TwilioSmsTest extends \PHPUnit\Framework\TestCase
    {

        public function tearDown()
        {
            parent::tearDown();
            m::close();
        }

        public function testSendTrue()
        {
            list($sut, $client) = $this->getSut();
            $client->messages = $client;
            $client->shouldReceive('create')->once()->with(
                '5555',
                ['body' => "Just A Note\nmessage\nURL: url", 'from' => '12345']
            );
            $this->assertTrue($sut->send('5555', ['msg' => 'message', 'url' => 'url']));
        }

        public function testSendWithTwilioExceptionIsFalse()
        {
            list($sut, $client, $l) = $this->getSut();
            $e = new TwilioException('twilio exception');
            $client->messages = $client;
            $client->shouldReceive('create')->once()->with(
                '5555',
                ['body' => "Just A Note\nmessage\nURL: url", 'from' => '12345']
            )
                   ->andThrow($e);
            $l->shouldReceive('error')->once()->with($e);
            $this->assertFalse($sut->send('5555', ['msg' => 'message', 'url' => 'url']));
        }

        protected function getSut()
        {
            $client = m::mock(Client::class);
            $l = m::mock(Log::class);
            $sut = new TwilioSms($client, $l, ['number' => '12345']);
            return [$sut, $client, $l];
        }
    }
}
