<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/10/17
 * Time: 11:03 AM
 */

namespace Tests\App\unit\Services\Token;

use App\Contracts\Token\Token;
use App\Services\Token\Store;
use Illuminate\Contracts\Session\Session;
use PHPUnit\Framework\TestCase;
use Mockery as m;

class StoreTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testGenerateReturnsToken()
    {
        list($sut, $t, $s) = $this->getSut();
        $t->shouldReceive('generate')->once()->with(16)->andReturn('token');
        $s->shouldReceive('get')->once()->with('jan_tokens', [])->andReturn([]);
        $s->shouldReceive('put')->once()->with('jan_tokens', ['1' => 'token']);
        $this->assertEquals('token', $sut->generate('1'));
    }

    public function testResetCallsForgetAndRegenerate()
    {
        list($sut, $t, $s) = $this->getSut();
        $s->shouldReceive('forget')->once()->with('jan_tokens');
        $s->shouldReceive('regenerate')->once();
        $this->assertNull($sut->reset());
    }

    public function testRetrieveGetsTokenFromSession()
    {
        list($sut, $t, $s) = $this->getSut();
        $s->shouldReceive('get')->once()->with('jan_tokens', [])->andReturn(['1' => 'token']);
        $this->assertEquals('token', $sut->retrieve('1'));
    }

    public function testVerifyIsFalseWhenNotMatching()
    {
        list($sut, $t, $s) = $this->getSut();
        $s->shouldReceive('get')->once()->with('jan_tokens', [])->andReturn(['1' => 'token']);
        $this->assertFalse($sut->verify('1', 'foo_token'));
    }

    public function testVerifyIsTrueWhenMatching()
    {
        list($sut, $t, $s) = $this->getSut();
        $s->shouldReceive('get')->once()->with('jan_tokens', [])->andReturn(['1' => 'token']);
        $this->assertTrue($sut->verify('1', 'token'));
    }

    protected function getSut()
    {
        $t = m::mock(Token::class);
        $s = m::mock(Session::class);
        $sut = new Store($t, $s);
        return [$sut, $t, $s];
    }
}
