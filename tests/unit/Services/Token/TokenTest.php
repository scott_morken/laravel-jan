<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/2/17
 * Time: 11:52 AM
 */

namespace Tests\App\unit\Services\Token;

use App\Services\Token\Token;

class TokenTest extends \PHPUnit\Framework\TestCase
{

    public function testLowerBoundLengthSetsMinimum()
    {
        $sut = $this->getSut(null);
        $this->assertEquals(8, $sut->getLength());
    }

    public function testUpperBoundLengthIsException()
    {
        $this->expectException(\OutOfBoundsException::class);
        $this->expectExceptionMessage('2048 is not a valid size.');
        $sut = $this->getSut(2048);
    }

    public function testToken()
    {
        $sut = $this->getSut();
        $token = $sut->generate();
        $this->assertGreaterThanOrEqual(32, strlen($token));
    }

    protected function getSut($length = 32)
    {
        return new Token($length);
    }
}
